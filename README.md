
![Alt text](images/nachifylogo.png)
**An educational tool that can be used to parse, generate and format NACHA files**


### Recently Added Features
- Parse returns and dishonored returns
- Select a file using the Nachify file explorer
- Users can now enter the name of files generated with the Nachify tool or press `ENTER` to use an auto-generated name
- Generate a return file from the entry of an ingested file
- Fields are auto-populated when a return file is created from an entry
- Fields with a value that is an aggregate of other fields will automatically update when the value of their aggregates parts are changed

## Getting Started

The repo already includes a test file. To get started follow the instructions below

### Install the Tool
- ```go install gitlab.com/ParkerG/nachify@latest```

### Run command
```nachify test.ach```

OR

```nachify``` (to select a file using the Nachify file explorer)

---

## Usage

### File Explorer
- Use the direction arrow to navigate through your directories
- ```ENTER``` to select a file or directory
- ```LEFT``` to back out of a directory

### Field Navigation
- Hit ```ENTER``` to enable field selection
- Use directional arrows to navigate to different fields

### Edit A Field
- Navigate to a field and hit ```CTRL + E``` to open the editor
>The editor will only allow values that are less than or equal to the length of the field that is being edited.
>Some fields will only allow numbers, or must start with spaces but this is a WIP.
- After entering a value hit ```ENTER``` to save changes
- Altered fields will have a grey background
- Hit ```CTRL + J``` to place focus back onto the field navigation component 

### Copy selected field to clipboard
- Navigate to the field you want to copy and hit ```CTRL + ]``` to copy to clipboard

### Copy one field to another
- Navigate to the field you want to copy and hit ```CTRL + D``` to copy, then navigate to the field you want to paste to and hit ```CTRL + V```
> Both fields must have the same length or the copy command will not work.


### Switch the values of two fields
- Navigate to the first field you want to swtich and hit ```CTRL + D``` to copy, then navigate to the second field and hit ```CTRL + S```
> Both fields must have the same length or the switch command will not work.

### Insert new entry into file
- Navigate to an entry field and hit ```CTRL + I```. This will duplicate the selected entry and adjust the batch and file totals to reflect the new entry

### Remove an entry from the file
- Navigate to an entry field and hit ```CTRL + R```. This will remove the selected entry and adjust the batch and file totals to reflect the new entry
- If the removed entry is the only one within the batch, the entire batch will be removed from the file

### Save a new file
- Hit ```CTRL + G``` to generate a new file from the tool.
> When prompted you may choose to enter a file name or just and enter in which case a file name will be automatically generated.
> Files are automatically saved to the```Generated_ACHs``` directory in your home folder. It will create it if it does not exist.
> Enter `#` as the filename to save the file to your clipboard
 
>- For original files that have been altered within Nachify, auto-generated filenames will be `gen_X.ach` where `X` is an integer that will increment with each newly added file.
> 
>- For files generated from Returns created within Nachify, the filename will be same as the original ingested file with `return_` prepended to it.
> For example, if the original file was name `test.ach`, the generated return file will be named `return_test.ach`


### Create a return file from an ingested file
- Hit ```CTRL + Y``` to generate a new file from the tool. Hit ```CTRL + R``` again to cycle back and forth between files
> NOTE: At this point returns can only be generated from a single entry. This means that you must select an ENTRY field (any field on a line starting with 6) before pressing ```CTRL + R```. Otherwise no return will be generated 

> Some return fields will auto-populate with the appropriate data based on the original entry

### Read a file from a stream
Files can be read from a stream by piping to the nachify command and using `-` in place of the file name.
- EXAMPLE: 
  - `echo  'contents of the ACH file' | nachify -`


  
> PRO TIP: If you have a terminal clipboard tool like **pbcopy** (Macs only) or **xclip** installed, you can pipe directly from
the paste command into the nachify tool like so: `pbpaste | nachify -`. Creating an alias for this command such as `alias nstream='pbpaste | nachify -'` allows you to quickly
copy a file to your clipboard and view it using the nachify tool. 

### Features in the works:
- Allow users to choose the directory of the generated file
- Tips/Hints section to provide additional information about a field when editing. e.g. (other related fields, content constraints such as numeric or alphameric etc.)
- Add ability add new batches, entries and addenda's
- Autocomplete
