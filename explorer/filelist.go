package explorer

import (
	tcell2 "github.com/gdamore/tcell/v2"
	"gitlab.com/ParkerG/nachify/settings"
	"io/ioutil"
	"log"
	"os"
	"path"
	"sort"
	"strings"
)

var currentPath string

type tpretty struct {
	filename string
	size     int64
	fgColor  tcell2.Color
	bgColor  tcell2.Color
}

var filelist struct {
	fullInfo []os.FileInfo
	pretty   []tpretty
}

var shouldShowHidden = false

func initFileList() {

	initialPath := settings.Get(settings.SetConfig, settings.KeyLastOpenDirectory).(string)
	if !doesDirectoryExist(initialPath) || !isDirectoryReadable(initialPath) {
		initialPath = settings.GetInitialDirectory()
	}
	setCurrentPath(initialPath)

	setPathWidgetText(initialPath)
	populateDirList()
}

func changeDirectory(path string) {
	if isDirectoryReadable(path) {
		setCurrentPath(path)
		setPathWidgetText(path)
		ReRenderExplorer(true)
	}
}

// populateDirList builds the list of elements
// in the selected path
func populateDirList() {
	filelist.fullInfo = []os.FileInfo{}
	filelist.pretty = []tpretty{}

	dirList, err := ioutil.ReadDir(currentPath)

	if err != nil {
		log.Fatal(err)
	}

	sort.Sort(SortByLowerCaseFilename(dirList))

	dirs := []os.FileInfo{}
	files := []os.FileInfo{}
	for _, file := range dirList {
		filename := file.Name()

		isHiddenFile := strings.HasPrefix(filename, ".")
		if (isHiddenFile && shouldShowHidden) || (!isHiddenFile) {

			if file.IsDir() {
				dirs = append(dirs, file)
			} else {
				files = append(files, file)
			}
		}
	}

	// Directories first, files after
	filelist.fullInfo = append(filelist.fullInfo, dirs...)
	filelist.fullInfo = append(filelist.fullInfo, files...)
}

func getPrettyList() []tpretty {
	colorifyDirList()
	return filelist.pretty
}

func colorifyDirList() {

	for _, file := range filelist.fullInfo {
		fgColor := tcell2.ColorWhite
		bgColor := tcell2.ColorBlack
		//path := path.Join(currentPath, file.Name())
		if file.IsDir() {
			fgColor = tcell2.ColorYellow
		}
		data := tpretty{file.Name(), file.Size(), fgColor, bgColor}
		filelist.pretty = append(filelist.pretty, data)
	}
}

func setCurrentPath(path string) {
	currentPath = path

	settings.Set(settings.SetConfig, settings.KeyLastOpenDirectory, path)
}

func isDirectoryReadable(dir string) bool {
	if _, err := ioutil.ReadDir(dir); err != nil {
		return false
	}
	return true
}

func doesDirectoryExist(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}

func getFilelistIndexOf(pathToFind string) int {
	for i, file := range filelist.fullInfo {
		if path.Join(currentPath, file.Name()) == pathToFind {
			return i
		}
	}
	return -1
}
