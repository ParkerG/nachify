package cmd

import (
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

type NachifyController struct {
	*NachifiedFile
}

type Controller interface {
	MoveUp()
	MoveDown()
	MoveLeft()
	MoveRight()
	GetSelectedField() NachifyField
	GetFieldByID(id int) NachifyField
	SetPrimaryView(view *tview.TextView)
	GetPrimaryView() *tview.TextView
	SetDefinitionView(view *tview.TextView)
	ResetDefinitionView(selectedField NachifyField)
	GetDefinitionView() *tview.TextView
	SetAlternateView(view *tview.TextView)
	GetAlternateView() *tview.TextView
	GetLegend() string
	NachifyFileWithStyle() string
	ResetView(content string)
	ViewInputHandler(event *tcell.EventKey)
	SaveNewFile()
}

func (nc *NachifyController) SetState(nf *NachifiedFile) {
	nc.NachifiedFile = nf
	app.SetFocus(nf.View)
}

func NewNachifyController(nf *NachifiedFile) NachifyController {
	nc := NachifyController{
		NachifiedFile: nf,
	}
	nf.SetController(&nc)
	return nc
}
