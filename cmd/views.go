package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/atotto/clipboard"
	"github.com/enescakir/emoji"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

const (
	Purple   = "#ff00ff"
	LtPurple = "#cc99ff"
	Red      = "#ff0000"
	HotPink  = "#ff007f"
	Blue     = "#0000ff"
	SkyBlue  = "#9999ff"
	Cyan     = "#00ffff"
	Yellow   = "#ffff00"
	LtYellow = "#ffff99"
	Green    = "#00ff00"
	LtGreen  = "#99ff99"
	Orange   = "#ff8000"
	LtOrange = "#ffcc99"
	White    = "#ffffff"
	Grey     = "#808080"
	LtGrey   = "#4a4a4a"
	Plain    = ""
)

var TPurple = NewTviewColorFunc(Purple)
var TLtPurple = NewTviewColorFunc(LtPurple)
var TRed = NewTviewColorFunc(Red)
var THotPink = NewTviewColorFunc(HotPink)
var TBlue = NewTviewColorFunc(Blue)
var TSkyBlue = NewTviewColorFunc(SkyBlue)
var TCyan = NewTviewColorFunc(Cyan)
var TYellow = NewTviewColorFunc(Yellow)
var TLtYellow = NewTviewColorFunc(LtYellow)
var TGreen = NewTviewColorFunc(Green)
var TLtGreen = NewTviewColorFunc(LtGreen)
var TOrange = NewTviewColorFunc(Orange)
var TLtOrange = NewTviewColorFunc(LtOrange)
var TWhite = NewTviewColorFunc(White)
var TGrey = NewTviewColorFunc(Grey)

var copiedValue string

var showingReturnView bool
var returnScreenView *tview.TextView
var primaryScreenView *tview.TextView
var editorScreenView *tview.InputField

func NewFileView(app *tview.Application, title string) *tview.TextView {
	view := tview.NewTextView().
		SetScrollable(true).
		SetDynamicColors(true).
		SetRegions(true).
		SetWrap(false).
		SetChangedFunc(func() {
			app.Draw()
		})

	view.SetBorder(true)
	view.SetTitle(title)
	return view
}

func NewDefinitionView(app *tview.Application, title string) *tview.TextView {
	v := tview.NewTextView().
		SetScrollable(true).
		SetDynamicColors(true).
		SetWordWrap(true).
		SetChangedFunc(func() {
			app.Draw()
		})

	v.SetBorder(true)
	v.SetTitle(fmt.Sprintf(" %s TERMINOLOGY ", emoji.OpenBook))
	return v
}

func NewLabelView(app *tview.Application, title string) *tview.TextView {
	v := tview.NewTextView().
		SetScrollable(true).
		SetDynamicColors(true).
		SetWordWrap(true).
		SetChangedFunc(func() {
			app.Draw()
		})

	v.SetBorder(true).SetTitleAlign(tview.AlignLeft)
	v.SetTitle(fmt.Sprintf(" %s  MAP [Ctrl + K] ", emoji.WorldMap))
	return v
}

func NewFlexView(fileView, definitionView, labelView *tview.TextView) *tview.Flex {
	fileNDefinitionView := tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexColumn).
			AddItem(fileView, 0, 1, true).
			AddItem(definitionView, 0, 1, false),
			0, 1, false)

	return tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(fileNDefinitionView, 0, 2, false).
			AddItem(labelView, 0, 1, false),
			0, 1, false)
}

func NewTipView(app *tview.Application) *tview.TextView {
	v := tview.NewTextView().
		SetScrollable(true).
		SetDynamicColors(true).
		SetWordWrap(true).
		SetChangedFunc(func() {
			app.Draw()
		})

	v.SetBorder(true)
	v.SetTitle(fmt.Sprintf(" %s  HELPFUL TIPS ", emoji.LightBulb))
	return v
}

func NewLogView(app *tview.Application) *tview.TextView {
	v := tview.NewTextView().
		SetScrollable(true).
		SetDynamicColors(true).
		SetWordWrap(true).
		SetChangedFunc(func() {
			app.Draw()
		})

	v.SetBorder(false)
	return v
}

func NewFlexEditor(inputView *tview.InputField, tipView, logView, fileView, definitionView *tview.TextView) *tview.Flex {
	fileNDefinitionView := tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexColumn).
			AddItem(fileView, 115, 0, true).
			AddItem(definitionView, 0, 1, false),
			0, 1, false)

	inputNLogView := tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(inputView, 0, 1, false).
			AddItem(logView, 0, 1, false),
			0, 1, false)

	editorLogTipView := tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexColumn).
			AddItem(inputNLogView, 115, 0, true).
			AddItem(tipView, 0, 1, false),
			0, 1, false)

	return tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(fileNDefinitionView, 0, 2, false).
			AddItem(editorLogTipView, 0, 1, false),
			0, 1, false)
}

func NewFlexEditorWithReturnView(inputView *tview.InputField, definitionView, logView, fileView, alternateView *tview.TextView) *tview.Flex {
	primaryNAlternateFileView := tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexColumn).
			AddItem(fileView, 115, 0, true).
			AddItem(alternateView, 0, 1, false),
			0, 1, false)

	editorNLogMessageView := tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(inputView, 0, 1, false).
			AddItem(logView, 0, 1, false),
			0, 1, false)

	editorNDefinitionView := tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexColumn).
			AddItem(editorNLogMessageView, 115, 0, true).
			AddItem(definitionView, 0, 1, false),
			0, 1, false)

	return tview.NewFlex().
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(primaryNAlternateFileView, 0, 2, false).
			AddItem(editorNDefinitionView, 0, 1, false),
			0, 1, false)
}

func NewEditorView(app *tview.Application, selectable NachifyField, logView *tview.TextView, fileView *tview.TextView, nf *NachifiedFile) *tview.InputField {
	length := selectable.GetLastPosition() - selectable.GetStartPosition() + 1
	updatedText := ""
	name := selectable.GetName()
	startValue := selectable.GetTextValue()

	v := tview.NewInputField().
		SetLabel(fmt.Sprintf("Enter a value for %s:  ", name)).
		SetFieldWidth(length).
		SetAcceptanceFunc(func(textToCheck string, lastChar rune) bool {
			pass := true
			text := ""
			if len(textToCheck) > length {
				text = ColorText(fmt.Sprintf("%s can only be %d characters long", name, length), Yellow) + "\n"
				pass = false
			}
			if selectable.GetContents() == NUMERIC {
				if ('a' <= lastChar && lastChar <= 'z') || ('A' <= lastChar && lastChar <= 'Z') {
					text += ColorText(fmt.Sprintf("%s can only contain numbers", name), Yellow) + "\n"
					pass = false
				}
			}
			if selectable.GetContents() == BTTTTAAAAC {
				if len(textToCheck) > 0 && textToCheck[0] != ' ' {
					text += ColorText(fmt.Sprintf("%s must start with a blank space (bTTTTAAAAC)", name), Yellow) + "\n"
					pass = false
				}
			}
			if !pass {
				logView.Clear()
				fmt.Fprint(logView, text)
			}
			return pass
		}).
		SetChangedFunc(func(text string) {
			updatedText = text
		}).
		SetAutocompleteFunc(func(current string) []string {
			l := len(current)

			if l == 0 {
				return []string{startValue}
			}
			ut := current[:l]
			st := startValue[:l]
			if ut == st {
				return []string{startValue}
			}
			return []string{}
		}).
		SetDoneFunc(func(key tcell.Key) {
			if key == tcell.KeyEscape {
				app.Stop()
			}
			if key == tcell.KeyEnter {
				// Changes to the entry Transaction Code field must be individually handled
				// since the transaction code links to certain (Credit or Debit) batch fields depending on the value
				if selectable.GetName() == TransactionCode {
					entry := selectable.(*EntryField).ParentEntry
					entry.ReassignDCLinks(updatedText)
				}

				selectable.SetValue(updatedText)
				selectable.SetBackgroundColor(LtGrey)

				updatedDoc := nf.NachifyFileWithStyle()
				fileView.Clear()
				fmt.Fprintf(fileView, "%s", updatedDoc)
				app.SetFocus(fileView)
			}
		})
	v.SetBorder(true).SetTitleAlign(tview.AlignLeft)
	v.SetTitle(fmt.Sprintf(" %s  EDITOR [Ctrl + K] ", emoji.Pencil))
	return v
}

func NewSaveFileView(app *tview.Application, logView *tview.TextView, fileView *tview.TextView, nf *NachifiedFile) *tview.InputField {
	filename := ""
	var defaultFileName string
	if nf.IsReturnFile() && inputFileName != "-" {
		defaultFileName = "return_" + inputFileName
	}
	v := tview.NewInputField().
		SetLabel("Enter the name for the new file or press ENTER to use an auto generated filename:").
		SetAcceptanceFunc(func(textToCheck string, lastChar rune) bool {
			parts := strings.Split(textToCheck, ".")
			numberOfParts := len(parts)
			if numberOfParts < 2 {
				return true
			}
			if numberOfParts == 2 {
				if len(parts[1]) == 0 {
					return true
				}
				if len(parts[1]) == 1 && lastChar == 'a' {
					logView.Clear()
					return true
				} else if len(parts[1]) == 2 && parts[1][1] == 'c' {
					logView.Clear()
					return true
				} else if len(parts[1]) == 3 && parts[1][2] == 'h' {
					ext := filepath.Ext(textToCheck)
					if ext != ".ach" {
						msg := NewWarningMsg("File name must end with a [::b].ach[::-] file extension")
						fmt.Fprint(logView, msg)
						return false
					}
					logView.Clear()
					return true
				}
				logView.Clear()
				msg := NewWarningMsg("File name must end with an [::b].ach[::-] file extension")
				fmt.Fprint(logView, msg)
				return false
			}
			return false
		}).
		SetChangedFunc(func(text string) {
			filename = text
		}).
		SetAutocompleteFunc(AutoCompleteFilename).
		SetDoneFunc(func(key tcell.Key) {
			if key == tcell.KeyEscape {
				app.Stop()
			}
			if key == tcell.KeyEnter {
				if filename != "" {
					ext := filepath.Ext(filename)
					if ext != ".ach" && filename != "#" {
						logView.Clear()
						msg := NewWarningMsg("File name must end with an [::b].ach[::-] file extension")
						fmt.Fprint(logView, msg)
						return
					}
				}
				if filename == "" && defaultFileName != "" {
					filename = defaultFileName
				}
				fullPath, err := nf.SaveNewFile(filename)
				if err != nil {
					fmt.Fprintf(logView, "%s", err.Error())
					return
				}

				if fullPath != "" {
					fmt.Fprint(logView, NewSuccessMsg("File successfully saved to "+fullPath))
					SaveFileNameToHistory(fullPath)
				}
				logView.Clear()
				app.SetFocus(fileView)
			}
		})
	v.SetBorder(true).SetTitleAlign(tview.AlignLeft)
	v.SetTitle(fmt.Sprintf(" %s  EDITOR [Ctrl + K] ", emoji.Pencil))
	return v
}

func NewReturnView(app *tview.Application, title string) *tview.TextView {
	view := tview.NewTextView().
		SetScrollable(true).
		SetDynamicColors(true).
		SetRegions(true).
		SetWrap(false).
		SetChangedFunc(func() {
			app.Draw()
		})

	view.SetBorder(true)
	view.SetTitle(title)

	//view.SetDoneFunc(FileViewInputHandler)
	return view
}

// When not in return view
func ShowEditorView(nf *NachifiedFile) {
	flex.Clear()
	logView := NewLogView(app)
	tipView := NewTipView(app)
	editorView := NewEditorView(app, nf.GetSelectedField(), logView, nf.View, nf)
	flexEditor := NewFlexEditor(editorView, tipView, logView, nf.View, nf.DefinitionView)
	editorScreenView = editorView
	app.SetRoot(flexEditor, true).SetFocus(editorView)
	app.Draw()
}

// When already in return view
func ShowEditorWithReturnView(nf *NachifiedFile) {
	flex.Clear()
	logView := NewLogView(app)
	editorView := NewEditorView(app, nf.GetSelectedField(), logView, nf.View, nf)

	var flexEditor *tview.Flex
	if nf.IsReturnFile() {
		flexEditor = NewFlexEditorWithReturnView(editorView, nf.DefinitionView, logView, nf.AlternateView, nf.View)
	} else {
		flexEditor = NewFlexEditorWithReturnView(editorView, nf.DefinitionView, logView, nf.View, nf.AlternateView)
	}
	editorScreenView = editorView
	app.SetRoot(flexEditor, true).SetFocus(editorView)
	app.Draw()
}

// When not in return view
func ShowSaveFileView(nf *NachifiedFile) {
	flex.Clear()
	logView := NewLogView(app)
	tipView := NewTipView(app)
	saveView := NewSaveFileView(app, logView, nf.View, nf)
	flexEditor := NewFlexEditor(saveView, tipView, logView, nf.View, nf.DefinitionView)
	app.SetRoot(flexEditor, true).SetFocus(saveView)
	app.Draw()
}

// When already in return view
func ShowSaveFileWithReturnView(nf *NachifiedFile) {
	flex.Clear()
	logView := NewLogView(app)
	saveView := NewSaveFileView(app, logView, nf.View, nf)

	var flexEditor *tview.Flex
	if nf.IsReturnFile() {
		flexEditor = NewFlexEditorWithReturnView(saveView, nf.DefinitionView, logView, nf.AlternateView, nf.View)
	} else {
		flexEditor = NewFlexEditorWithReturnView(saveView, nf.DefinitionView, logView, nf.View, nf.AlternateView)
	}
	app.SetRoot(flexEditor, true).SetFocus(saveView)
	app.Draw()
}

func ResetDefinitionView(selectedField NachifyField) {
	definition := FormatDefinition(selectedField.GetRecordPrefix(), selectedField.GetPosition(), selectedField.GetDefinition(), selectedField.GetLength())
	viewHeader := ColorText(selectedField.GetName(), selectedField.GetTextColor())
	definitionView.Clear()
	fmt.Fprintf(definitionView, "%s", definition)
	definitionView.SetTitle(" [::b]" + viewHeader + "[::-] [Ctrl + L] ")
	definitionView.ScrollToBeginning()
}

func AutoCompleteFilename(currentText string) []string {
	if len(currentText) == 0 {
		filenames := struct {
			Names []string `json:"names"`
		}{Names: make([]string, 0, 5)}

		homeDir, _ := os.UserHomeDir()
		configFile := filepath.Join(homeDir + "/nachify_configs/filenames.json")

		nameHistory, err := ioutil.ReadFile(configFile)
		ExitOnErr(err)

		err = json.Unmarshal(nameHistory, &filenames)
		ExitOnErr(err)

		return filenames.Names
	}

	return []string{}
}

func SaveFileNameToHistory(fullPath string) {
	_, fn := filepath.Split(fullPath)
	_ = clipboard.WriteAll(fn)
	newName := []string{fn}
	filenames := struct {
		Names []string `json:"names"`
	}{Names: make([]string, 0, 5)}

	homeDir, err := os.UserHomeDir()
	ExitOnErr(err)

	configFile := filepath.Join(homeDir + "/nachify_configs/filenames.json")
	nameHistory, err := ioutil.ReadFile(configFile)
	ExitOnErr(err)

	err = json.Unmarshal(nameHistory, &filenames)
	ExitOnErr(err)

	if len(filenames.Names) >= 5 {
		filenames.Names = append(newName, filenames.Names[0:5]...)
	} else {
		filenames.Names = append(newName, filenames.Names...)
	}

	data, _ := json.Marshal(filenames)

	_ = ioutil.WriteFile(configFile, data, 0666)
}
