package cmd

import "fmt"

type Addenda99Type int

const (
	Return Addenda99Type = iota
	Dishonored
	Contested
)

var ReturnFieldContext = []FieldContext{
	FieldIDToContext["7_RecordTypeCode"],
	FieldIDToContext["7_AddendaTypeCode"],
	FieldIDToContext["7_ReturnReasonCode"],
	FieldIDToContext["7_OriginalEntryTraceNo"],
	FieldIDToContext["7_DateOfDeath"],
	FieldIDToContext["7_OriginalReceivingDFI"],
	FieldIDToContext["7_AddendaInfoOne"],
	FieldIDToContext["7_TraceNo"],
}

var DishonoredReturnFieldContext = []FieldContext{
	FieldIDToContext["7_RecordTypeCode"],
	FieldIDToContext["7_AddendaTypeCode"],
	FieldIDToContext["7_ReturnReasonCode"],
	FieldIDToContext["7_OriginalEntryTraceNo"],
	FieldIDToContext["7_ReservedOne"],
	FieldIDToContext["7_OriginalReceivingDFI"],
	FieldIDToContext["7_ReservedTwo"],
	FieldIDToContext["7_ReturnTraceNo"],
	FieldIDToContext["7_ReturnSettlementDate"],
	FieldIDToContext["7_ReturnReasonCodeDishonored"],
	FieldIDToContext["7_AddendaInfoDishonored"],
	FieldIDToContext["7_TraceNo"],
}

var ContestedReturnFieldContext = []FieldContext{
	FieldIDToContext["7_RecordTypeCode"],
	FieldIDToContext["7_AddendaTypeCode"],
	FieldIDToContext["7_ContestedDishonoredReturnReasonCode"],
	FieldIDToContext["7_OriginalEntryTraceNo"],
	FieldIDToContext["7_OriginalEntryReturnDate"],
	FieldIDToContext["7_OriginalReceivingDFI"],
	FieldIDToContext["7_OriginalSettlementDate"],
	FieldIDToContext["7_ReturnTraceNo"],
	FieldIDToContext["7_ReturnSettlementDate"],
	FieldIDToContext["7_ReturnReasonCodeDishonored"],
	FieldIDToContext["7_DishonoredReturnTraceNo"],
	FieldIDToContext["7_DishonoredReturnSettlementDate"],
	FieldIDToContext["7_DishonoredReturnReasonCodeDishonored"],
	FieldIDToContext["7_ReservedThree"],
	FieldIDToContext["7_TraceNo"],
}

type Addenda99 struct {
	AddendaText   string
	Type          Addenda99Type
	Entry         *Entry
	AddendaFields []*AddendaField
	EntryFields   []*EntryField
}

type AddendaField struct {
	PlainText   string
	StyledText  string
	ParentEntry *Entry
	*FieldData
	*StyleData
	*SelectionData
	BatchHeaderLine  int
	BatchControlLine int
}

func NewAddenda99(entry *Entry, addendaRecord string) *Addenda99 {
	addenda := &Addenda99{
		AddendaText:   addendaRecord,
		Entry:         entry,
		AddendaFields: NewReturnFields(addendaRecord, entry),
	}
	switch addendaRecord[3:6] {
	case "R01", "R02", "R03", "R04", "R05", "R06", "R07", "R08", "R09", "R10", "R12", "R13", "R14", "R15", "R16", "R17", "R18",
		"R19", "R20", "R21", "R22", "R23", "R24", "R25", "R26", "R27", "R28", "R29", "R30", "R31", "R32", "R33", "R34", "R35",
		"R36", "R37", "R38", "R39":
		addenda.AddendaFields = NewReturnFields(addendaRecord, entry)
		addenda.Type = Return
	case "R61", "R62", "R67", "R68", "R69", "R70":
		addenda.AddendaFields = NewDishonoredReturnFields(addendaRecord, entry)
		addenda.Type = Dishonored
	case "R74":
		addenda.AddendaFields = NewContestedReturnFields(addendaRecord, entry)
		addenda.Type = Contested
	}
	return addenda
}

func NewReturnFields(addendaRecord string, parentEntry *Entry) []*AddendaField {
	var addendaFields []*AddendaField

	for _, ac := range ReturnFieldContext {
		addendaField := &AddendaField{
			ParentEntry: parentEntry,
		}
		addendaField.FieldData = NewFieldData(addendaField, ac, addendaRecord)
		addendaField.StyleData = NewStyleData(ac.Color)
		addendaField.SelectionData = new(SelectionData)
		//NewNachifySelection(addendaField)
		addendaFields = append(addendaFields, addendaField)
	}
	return addendaFields
}

func NewDishonoredReturnFields(addendaRecord string, parentEntry *Entry) []*AddendaField {
	var addendaFields []*AddendaField

	for _, ac := range DishonoredReturnFieldContext {
		addendaField := &AddendaField{
			ParentEntry: parentEntry,
		}
		addendaField.FieldData = NewFieldData(addendaField, ac, addendaRecord)
		addendaField.StyleData = NewStyleData(ac.Color)
		addendaField.SelectionData = new(SelectionData)
		//NewNachifySelection(addendaField)
		addendaFields = append(addendaFields, addendaField)
	}
	return addendaFields
}

func NewContestedReturnFields(addendaRecord string, parentEntry *Entry) []*AddendaField {
	var addendaFields []*AddendaField

	for _, ac := range ContestedReturnFieldContext {
		addendaField := &AddendaField{
			ParentEntry: parentEntry,
		}
		addendaField.FieldData = NewFieldData(addendaField, ac, addendaRecord)
		addendaField.StyleData = NewStyleData(ac.Color)
		addendaField.SelectionData = new(SelectionData)
		//NewNachifySelection(addendaField)
		addendaFields = append(addendaFields, addendaField)
	}
	return addendaFields
}

func CreateAddendaLegend() string {
	var legend string
	for _, af := range ReturnFieldContext {
		legend += " " + ColorText(af.Label, af.Color)
	}
	return legend
}

func (af *AddendaField) RenderStyle() string {
	af.StyledText = *af.Value
	if af.FormatFunc != nil {
		af.StyledText = af.FormatFunc(af.Length, af.StyledText)
	}
	if af.SelectionData.SelectionNo != 0 {
		af.StyledText = fmt.Sprintf(`["%d"]%s[""]`, af.SelectionData.SelectionNo, af.StyledText)
	}
	if af.Effects > 0 {
		effectsPrefix := fmt.Sprintf("[%s:%s:%s%s]", af.TextColor, af.BackgroundColor, af.BoldEffect, af.UnderlineEffect)
		effectsSuffix := "[-:-:-]"
		af.StyledText = fmt.Sprintf("%s%s%s", effectsPrefix, af.StyledText, effectsSuffix)
	}
	return af.StyledText
}

func (af *AddendaField) RenderPlain() string {
	af.PlainText = *af.Value
	if af.FormatFunc != nil {
		af.PlainText = af.FormatFunc(af.Length, af.PlainText)
	}
	if af.SelectionData.SelectionNo != 0 {
		return af.PlainText
	}
	return ""
}

func AddAddenda99(selected NachifyField) {
	if selectedEntry, ok := selected.(*EntryField); ok {
		parentEntry := selectedEntry.ParentEntry
		originalTraceNo := parentEntry.FindEntryFieldByName(TraceNo).Value
		originalDFI := parentEntry.FindEntryFieldByName(RdfiRoutingNo).Value
		addendaLine := fmt.Sprintf("799R01%s000000%s                                            ADDENDATRACE_NO", *originalTraceNo, *originalDFI)
		parentEntry.Addenda99 = NewAddenda99(parentEntry, addendaLine)
	}
}

func (ad *Addenda99) FindAddendaFieldByName(name string) *AddendaField {
	for _, field := range ad.AddendaFields {
		if field.Name == name {
			return field
		}
	}
	return nil
}
