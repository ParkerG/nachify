package cmd

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/enescakir/emoji"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/ParkerG/nachify/explorer"
)

var cfgFile string

var initialField NachifyField

var fileView *tview.TextView
var returnView *tview.TextView

var app *tview.Application
var definitionView *tview.TextView
var labelView *tview.TextView
var flex *tview.Flex

var controller NachifyController
var inputFileName string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "nachify",
	Short: "A tool for parsing NACHA files",
	Long: `NACHA guidelines are long, detailed and occasionally confusing. This tool
can be used to parse NACHA files and provide additional information about
the fields and specifications in accordance with NACHA guidelines.`,
	TraverseChildren: true,
	Run: func(cmd *cobra.Command, args []string) {

		if len(args) > 0 {
			inputFileName = args[0]
			if inputFileName == "-" {
				Nachify(os.Stdin, nil)
			} else {
				ext := filepath.Ext(inputFileName)
				if ext != ".ach" {
					fmt.Println("File type must have extension .ach")
					os.Exit(1)
				}
				file, err := os.Open(inputFileName)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				Nachify(file, nil)
			}
		} else {
			explorer.Init(Nachify)
		}

	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.nachify.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".nachify" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".nachify")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

func SetBoolFlags(fieldMap map[string]FieldContext, setFlag func(*bool, string, bool, string)) {
	for k, v := range fieldMap {
		setFlag(v.FlagSet, k, false, v.Usage)
	}
}

type ColorFunc func(string) string

func NewTviewColorFunc(color string) ColorFunc {
	return func(word string) string {
		return fmt.Sprintf("[%s::]%s[%s::]", color, word, White)
	}
}

var colorizeLimit = map[string]bool{}

func Nachify(reader io.Reader, application *tview.Application) {
	if application == nil {
		app = tview.NewApplication()
	} else {
		app = application
	}

	fileView = NewFileView(app, fmt.Sprintf(" %s NACHA FILE [Ctrl + J] ", emoji.OpenFileFolder))
	definitionView = NewDefinitionView(app, fmt.Sprintf(" %s TERMINOLOGY ", emoji.OpenBook))
	labelView = NewLabelView(app, fmt.Sprintf(" %s MAP [Ctrl + K] ", emoji.WorldMap))

	//SetLimits()

	s := bufio.NewScanner(reader)

	//var label string
	//lines, label = GenerateNachaFile(s)

	file := NewFile(s)
	nachifiedFile := NewNachifiedFile(file)
	nachifiedFile.SetPrimaryView(fileView)

	controller = NewNachifyController(nachifiedFile)

	controller.SetDefinitionView(definitionView)
	controllerLegend := controller.GetLegend()
	fmt.Fprintf(labelView, "%s", controllerLegend)

	flex = NewFlexView(controller.GetPrimaryView(), controller.GetDefinitionView(), labelView)

	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		// Anything handled here will be executed on the main thread
		if event.Key() == tcell.KeyEscape {
			app.Stop()
			return nil
		}
		if event.Key() == tcell.KeyCtrlI {
			selectedField := controller.NachifiedFile.GetSelectedField()
			if selectedEntry, ok := selectedField.(*EntryField); ok {
				previousIndex := controller.NachifiedFile.SelectionIndex
				go func() {
					file = InsertNewEntry(selectedEntry)
					flex.Clear()
					fileView.Clear()
					labelView.Clear()
					definitionView.Clear()
					newNachifiedFile := NewNachifiedFile(file)

					newNachifiedFile.SetPrimaryView(fileView)
					controller.NachifiedFile = newNachifiedFile
					newNachifiedFile.SetController(&controller)
					newNachifiedFile.SelectionIndex = previousIndex
					controller.SetDefinitionView(definitionView)
					controllerLegend = controller.GetLegend()
					fmt.Fprintf(labelView, "%s", controllerLegend)
					flex = NewFlexView(controller.GetPrimaryView(), controller.GetDefinitionView(), labelView)
					app.SetRoot(flex, true).SetFocus(controller.GetPrimaryView())
					app.Draw()
				}()
			}
		}
		// TODO: This function needs to update the file batch count and file entry/addenda count
		if event.Key() == tcell.KeyCtrlR {
			selectedField := controller.NachifiedFile.GetSelectedField()
			if selectedEntry, ok := selectedField.(*EntryField); ok {
				previousIndex := controller.NachifiedFile.SelectionIndex
				go func() {
					file = RemoveEntry(selectedEntry)
					flex.Clear()
					fileView.Clear()
					labelView.Clear()
					definitionView.Clear()
					newNachifiedFile := NewNachifiedFile(file)

					newNachifiedFile.SetPrimaryView(fileView)
					controller.NachifiedFile = newNachifiedFile
					newNachifiedFile.SetController(&controller)
					newNachifiedFile.SelectionIndex = previousIndex
					controller.SetDefinitionView(definitionView)
					controllerLegend = controller.GetLegend()
					fmt.Fprintf(labelView, "%s", controllerLegend)
					flex = NewFlexView(controller.GetPrimaryView(), controller.GetDefinitionView(), labelView)
					app.SetRoot(flex, true).SetFocus(controller.GetPrimaryView())
					app.Draw()
				}()
			}
		}
		if controller.GetPrimaryView().HasFocus() {
			controller.ViewInputHandler(event)
			return nil
		}

		switch event.Key() {
		case tcell.KeyCtrlL:
			if returnView != nil {
				app.SetFocus(returnView)
			}
			app.SetFocus(controller.GetDefinitionView())
			return nil
		case tcell.KeyCtrlJ:
			app.SetFocus(controller.GetPrimaryView())
			return nil
		case tcell.KeyCtrlK:
			app.SetFocus(labelView)
			return nil
		}
		return event
	})

	if err := app.SetRoot(flex, true).SetFocus(controller.GetPrimaryView()).Run(); err != nil {
		panic(err)
	}
}

func ColorText(word string, color string) string {
	return fmt.Sprintf("[%s::]%s[%s::]", color, word, White)
}

func SetLimits() {
	for k := range FieldIDToContext {
		if *FieldIDToContext[k].FlagSet {
			colorizeLimit[k] = true
		}
	}
}
