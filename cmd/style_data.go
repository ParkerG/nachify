package cmd

type BoldConstant string
type UnderlineConstant string

const (
	BoldConst      BoldConstant      = "b"
	UnderlineConst UnderlineConstant = "u"
)

type StyleData struct {
	TextColor       string
	BackgroundColor string
	BoldEffect      BoldConstant
	UnderlineEffect UnderlineConstant
	Effects         int
}

func (sd *StyleData) DeepCopy() StyleData {
	return StyleData{
		TextColor:       sd.TextColor,
		BackgroundColor: sd.BackgroundColor,
		BoldEffect:      sd.BoldEffect,
		UnderlineEffect: sd.UnderlineEffect,
		Effects:         sd.Effects,
	}
}

// NewStyleData sets the look of a field such color, background color, and text style
// An effect value greater than one means some type of styling needs to be applied
func NewStyleData(color string) *StyleData {
	return &StyleData{
		TextColor: color,
		Effects:   1,
	}
}

func (sd *StyleData) GetTextColor() string {
	return sd.TextColor
}

func (sd *StyleData) SetBackgroundColor(color string) *StyleData {
	sd.BackgroundColor = color
	sd.Effects++
	return sd
}

// Bold sets bold styling for a text field
func (sd *StyleData) Bold() *StyleData {
	sd.BoldEffect = BoldConst
	sd.Effects++
	return sd
}

// Underline will underline a text field
func (sd *StyleData) Underline() *StyleData {
	sd.UnderlineEffect = UnderlineConst
	sd.Effects++
	return sd
}
