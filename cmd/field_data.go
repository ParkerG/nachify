package cmd

import (
	"strconv"
	"strings"
)

type FieldData struct {
	Parent         NachifyField
	Value          *string
	IntValue       *int
	MirroredField  NachifyField
	AggregateParts []NachifyField `json:"-"`
	Aggregators    []NachifyField `json:"-"`
	Name           string
	Label          string
	RecordPrefix   string
	RecordID       string
	Position       string
	StartPos       int
	LastPos        int
	Length         int
	Definition     *string
	Contents       string
	Format         Format `json:"-"`
	FormatFunc     func(length int, text string) string
	PreviousValues []string
}

// NewFieldData generates the relative contextual information about the field. For example:
// What type of field is it? A file record field, batch header field, entry field etc...
// What are the contents and format of the field? Alphanumeric, Numeric etc...
// What is this fields position(1-based index) on the line where it is found?
// What definition is this field bound to?
func NewFieldData(parent NachifyField, fc FieldContext, line string) *FieldData {
	fd := FieldData{
		Parent:       parent,
		Name:         fc.Name,
		Label:        fc.Label,
		RecordPrefix: fc.RecordPrefix,
		RecordID:     fc.RecordPrefix + "_" + fc.Label,
		Position:     fc.Position,
		Definition:   &fc.Definition,
		Contents:     fc.Contents,
		Format:       fc.Format,
	}

	if fd.Format == LeftZeroPadded {
		fd.FormatFunc = LeftPaddedZeroes
	}
	if fd.Format == RightSpacePadded {
		fd.FormatFunc = RightPaddedSpaces
	}

	fd.Length, fd.StartPos, fd.LastPos = GetLengthAndPositions(fc.Position)

	fd.Value, fd.IntValue = GetTextValueFromPosition(line, fd.Position)

	return &fd
}

func (fd *FieldData) GetFieldData() *FieldData {
	return fd
}

func (fd *FieldData) SetValue(value string) NachifyField {
	fd.Value = &value
	if fd.Contents == NUMERIC {
		intValue, _ := strconv.Atoi(value)
		fd.IntValue = &intValue
	}
	DoRecursiveRecalculation(fd.Aggregators)
	fd.RecalculateMirroredField()
	return fd.Parent
}

// RecalculateValue is called whenever the value of an AggregatePart changes
func (fd *FieldData) RecalculateValue() {
	intValue := 0
	stringValue := "0"
	if len(fd.AggregateParts) > 0 {
		for _, part := range fd.AggregateParts {
			intValue += part.GetIntValue()
		}
		fd.IntValue = &intValue
		stringValue = strconv.Itoa(intValue)
		fd.Value = &stringValue
	} else {
		fd.IntValue = &intValue
		fd.Value = &stringValue
	}
}

func DoRecursiveRecalculation(aggregators []NachifyField) {
	if len(aggregators) > 0 {
		for _, ag := range aggregators {
			ag.RecalculateValue()
			DoRecursiveRecalculation(ag.GetAggregators())
		}
	}
}

func (fd *FieldData) RecalculateMirroredField() {
	if fd.MirroredField != nil {
		fd.MirroredField.GetFieldData().Value = fd.Value
		fd.MirroredField.SetBackgroundColor(LtGrey)
	}
}

// Aggregate parts reference other fields whose value make up the sum
// of the corresponding fields value

//
func (fd *FieldData) MirrorField(field NachifyField) {
	fd.MirroredField = field
}

func (fd *FieldData) AddAggregatePart(part NachifyField) {
	fd.AggregateParts = append(fd.AggregateParts, part)
}

func (fd *FieldData) GetAggregateParts() []NachifyField {
	return fd.AggregateParts
}

func (fd *FieldData) RemoveAggregatePart(part NachifyField) {
	newParts := []NachifyField{}
	for _, ag := range fd.AggregateParts {
		if ag.GetSelectionNo() != part.GetSelectionNo() {
			newParts = append(newParts, ag)
		}
	}
	fd.AggregateParts = newParts
}

// Aggregators are fields whose value is the sum of other fields
// that reference the aggregator.
// Example: Field C is the sum of Field A and Field B
// Field C is an aggregator.
// Field A and Field B have a reference to Field C. This allows Field C to be updated
// if their value changes

func (fd *FieldData) InsertAggregator(aggregator NachifyField) {
	fd.Aggregators = append(fd.Aggregators, aggregator)
}

func (fd *FieldData) GetAggregators() []NachifyField {
	return fd.Aggregators
}

func (fd *FieldData) RemoveAggregator(aggregator NachifyField) {
	newAggregators := []NachifyField{}
	for _, ag := range fd.Aggregators {
		if ag.GetSelectionNo() != aggregator.GetSelectionNo() {
			newAggregators = append(newAggregators, ag)
		}
	}
	fd.Aggregators = newAggregators
}

func (fd *FieldData) GetContents() string {
	return fd.Contents
}

func (fd *FieldData) GetDefinition() string {
	return *fd.Definition
}

func (fd *FieldData) GetIntValue() int {
	return *fd.IntValue
}

func (fd *FieldData) GetTextValue() string {
	return *fd.Value
}

func (fd *FieldData) GetLength() int {
	return fd.Length
}

func (fd *FieldData) GetStartPosition() int {
	return fd.StartPos
}

func (fd *FieldData) GetLastPosition() int {
	return fd.LastPos
}

func (fd *FieldData) GetName() string {
	return fd.Name
}

func (fd *FieldData) GetPosition() string {
	return fd.Position
}

func (fd *FieldData) GetRecordPrefix() string {
	return fd.RecordPrefix
}

func GetLengthAndPositions(position string) (length, startPos, endPos int) {
	indices := strings.Split(position, "-")
	if len(indices) == 1 {
		pos, _ := strconv.Atoi(indices[0])
		return 1, pos, pos
	}

	startPos, _ = strconv.Atoi(indices[0])
	start := startPos - 1
	endPos, _ = strconv.Atoi(indices[1])
	length = endPos - start
	return length, startPos, endPos
}
