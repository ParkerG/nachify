package cmd

import (
	"bufio"
	"fmt"
	"log"
)

const (
	FileControlRecord  = '9'
	BatchControlRecord = '8'
)

const (
	BatchCountIndex = iota + 1
	_
	FileEntryAddendaCountIndex
	_
	TotalFileDebitIndex
	TotalFileCreditIndex
)

var FileHeaderFieldContext = []FieldContext{
	FieldIDToContext["1_RecordTypeCode"],
	FieldIDToContext["1_PriorityCode"],
	FieldIDToContext["1_ImmediateDestination"],
	FieldIDToContext["1_ImmediateOrigin"],
	FieldIDToContext["1_FileCreationDate"],
	FieldIDToContext["1_FileCreationTime"],
	FieldIDToContext["1_FileIDModifier"],
	FieldIDToContext["1_RecordSize"],
	FieldIDToContext["1_BlockingFactor"],
	FieldIDToContext["1_FormatCode"],
	FieldIDToContext["1_ImmediateDestinationName"],
	FieldIDToContext["1_ImmediateOriginName"],
	FieldIDToContext["1_ReferenceCode"],
}

var FileControlFieldContext = []FieldContext{
	FieldIDToContext["9_RecordTypeCode"],
	FieldIDToContext["9_BatchCount"],
	FieldIDToContext["9_BlockCount"],
	FieldIDToContext["9_EntryAddendaCount"],
	FieldIDToContext["9_EntryHash"],
	FieldIDToContext["9_TotalFileDebitAmount"],
	FieldIDToContext["9_TotalFileCreditAmount"],
	FieldIDToContext["9_Reserved"],
}

type File struct {
	FileHeaderFields   []*FileField
	Batches            []*Batch
	FileControlFields  []*FileField
	FileHeaderLine     int
	FileControlLine    int
	TotalBatchCount    int
	TotalEntryCount    int
	TotalReturnEntries int
	TotalCredits       string
	TotalDebits        string
}

// NewFile consumes a raw ach file from bufio.scanner and creates the relationships between
// the file, batches and entries and the individual fields contains within them.
func NewFile(s *bufio.Scanner) *File {
	file := &File{}

	var firstDigit byte

	// Read the FileHeaderRecord
	s.Scan()

	firstLine := s.Text()

	if len(firstLine) > 94 {
		var err error
		s, err = newSplitScanner(firstLine)
		if err != nil {
			log.Fatal(err)
		}
		s.Scan()
		firstLine = s.Text()
	}

	fileHeaderRecord := ExtendWhiteSpace(firstLine, 94)
	file.FileHeaderFields = NewFileHeaderFields(fileHeaderRecord)

	// Go to the next line which will be a BatchHeaderRecord
	s.Scan()

	// Grab the specific record/line
	record := s.Text()
	firstDigit = record[0]
	// Create all batches, entries and addenda fields
	var batchAndEntryRecords []string

	// if firstDigit is 9, we've reached the end of th efile
	for firstDigit != FileControlRecord {
		// We append everything except the FileControlRecord
		batchAndEntryRecords = append(batchAndEntryRecords, ExtendWhiteSpace(record, 94))
		// Clear batch and entry records when we get to the end of a batch
		if firstDigit == BatchControlRecord {
			batch := NewBatch(file, batchAndEntryRecords)
			file.Batches = append(file.Batches, batch)
			batchAndEntryRecords = []string{}
		}
		s.Scan()
		record = s.Text()
		firstDigit = record[0]
	}

	// Create file control fields
	fileControlRecord := ExtendWhiteSpace(record, 94)
	file.FileControlFields = NewFileControlFields(fileControlRecord)
	file.SetFileTotals()
	return file
}

//func NewReturnFile(entry *Entry) *File {
//
//}

type FileField struct {
	PlainText  string
	StyledText string
	*FieldData
	*StyleData
	*SelectionData
	FileHeaderLine  int
	FileControlLine int
}

func (f *File) FileHeaderFieldValueByIndex(index int) string {
	return f.FileHeaderFields[index].GetTextValue()
}

func (f *File) FileControlFieldValueByIndex(index int) string {
	return f.FileControlFields[index].GetTextValue()
}

// SetFileTotals is used to display statistics about the file
func (f *File) SetFileTotals() {
	f.TotalBatchCount = len(f.Batches)
	for _, batch := range f.Batches {
		entries, returnEntries := batch.GetEntryCounts()
		f.TotalEntryCount += entries
		f.TotalReturnEntries += returnEntries
	}
	f.TotalCredits = f.FileControlFieldValueByIndex(TotalFileCreditIndex)
	f.TotalDebits = f.FileControlFieldValueByIndex(TotalFileDebitIndex)
}

func NewFileHeaderFields(fileHeaderRecord string) []*FileField {
	var fileFields []*FileField

	for _, fc := range FileHeaderFieldContext {
		fileField := &FileField{}
		fileField.FieldData = NewFieldData(fileField, fc, fileHeaderRecord)
		fileField.StyleData = NewStyleData(fc.Color)
		fileField.SelectionData = new(SelectionData)
		//NewNachifySelection(fileField)
		fileFields = append(fileFields, fileField)
	}
	return fileFields
}

func CreateFileHeaderLegend() string {
	var legend string
	for _, fh := range FileHeaderFieldContext {
		legend += " " + ColorText(fh.Label, fh.Color)
	}
	return legend
}

func NewFileControlFields(fileControlRecord string) []*FileField {
	var fileFields []*FileField

	for _, fc := range FileControlFieldContext {
		fileField := &FileField{}
		fileField.FieldData = NewFieldData(fileField, fc, fileControlRecord)
		fileField.StyleData = NewStyleData(fc.Color)
		fileField.SelectionData = new(SelectionData)
		//NewNachifySelection(fileField)
		fileFields = append(fileFields, fileField)
	}
	return fileFields
}

func CreateFileControlLegend() string {
	var lg string
	for _, fc := range FileControlFieldContext {
		lg += " " + ColorText(fc.Label, fc.Color)
	}
	return lg
}

func (ff *FileField) RenderStyle() string {
	ff.StyledText = *ff.Value
	if ff.FormatFunc != nil {
		ff.StyledText = ff.FormatFunc(ff.Length, ff.StyledText)
	}
	if ff.SelectionData.SelectionNo != 0 {
		ff.StyledText = fmt.Sprintf(`["%d"]%s[""]`, ff.SelectionData.SelectionNo, ff.StyledText)
	}
	if ff.Effects > 0 {
		effectsPrefix := fmt.Sprintf("[%s:%s:%s%s]", ff.TextColor, ff.BackgroundColor, ff.BoldEffect, ff.UnderlineEffect)
		effectsSuffix := "[-:-:-]"
		ff.StyledText = fmt.Sprintf("%s%s%s", effectsPrefix, ff.StyledText, effectsSuffix)
	}
	return ff.StyledText
}

func (ff *FileField) RenderPlain() string {
	ff.PlainText = *ff.Value
	if ff.FormatFunc != nil {
		ff.PlainText = ff.FormatFunc(ff.Length, ff.PlainText)
	}
	if ff.SelectionData.SelectionNo != 0 {
		return ff.PlainText
	}
	return ""
}

func (f *File) GetFileFieldByName(name string) *FileField {
	for _, fhf := range f.FileHeaderFields {
		if fhf.Name == name {
			return fhf
		}
	}
	for _, fcf := range f.FileControlFields {
		if fcf.Name == name {
			return fcf
		}
	}
	return nil
}

func (f *File) GetBatchAndEntryByTraceNo(traceNo string) (*Batch, *Entry) {
	for _, batch := range f.Batches {
		entry := batch.GetEntryByTraceNo(traceNo)
		if entry != nil {
			return batch, entry
		}
	}
	return nil, nil
}

func (f *File) GetBatchByODFI(routingNo string) *Batch {
	for _, batch := range f.Batches {
		result, _ := batch.GetBatchFieldByName(OriginatingDFI)
		if result != nil && result.GetTextValue() == routingNo {
			return batch
		}
	}
	return nil
}
