package cmd

import (
	"github.com/goombaio/namegenerator"
	"strconv"
	"strings"
	"time"
)

var returnType = []string{"R01", "R02", "R03", "R04", "R10", "R16", "R24"}

func RunReturnEntryAssistant(fromEntry *Entry, toFile *NachifiedFile) {
	odfi, _ := fromEntry.Batch.GetBatchFieldByName(OriginatingDFI)
	originalSender := odfi.GetTextValue()                                            // Goes in RDFI Field of return entry
	originalReceiver := fromEntry.FindEntryFieldByName(RdfiRoutingNo).GetTextValue() // Goes in original DFI field of Addenda and the ODFI field of the batch containing the return entry
	originalTraceNo := fromEntry.FindEntryFieldByName(TraceNo).GetTextValue()        // Goes in original trace number of Addenda
	generatedReturnBatch, generatedReturnEntry := toFile.File.GetBatchAndEntryByTraceNo(originalTraceNo)
	generatedReturnAddenda := generatedReturnEntry.Addenda99

	returnTraceNo := NewRandomTraceNumber(originalReceiver) // This will be trace number of addenda and return entry. First 8 digits must match routing number of sender (aka the original receiver)
	returnBatchNo := NewRandomBatchNumber()                 // This is goes is batch number field of the batch header and batch control of the return entry
	returnBatchCompanyID := NewRandomBatchCompanyID()       // This is goes is batch company ID field of the batch header and batch control of the return entry

	generatedReturnAddenda.FindAddendaFieldByName(OriginalEntryTraceNo).SetValue(originalTraceNo).SetBackgroundColor(LtGrey)

	generatedReturnAddenda.FindAddendaFieldByName(OriginalReceivingDFI).SetValue(originalReceiver).SetBackgroundColor(LtGrey)
	generatedReturnEntry.FindEntryFieldByName(RdfiRoutingNo).SetValue(originalSender).SetBackgroundColor(LtGrey)
	generatedReturnEntry.FindEntryFieldByName(AddendaRecordIndicator).SetValue("1").SetBackgroundColor(LtGrey)

	checkDigit := ValidateRoutingNumber(originalSender)
	cdString := strconv.Itoa(checkDigit)
	generatedReturnEntry.FindEntryFieldByName(CheckDigit).SetValue(cdString).SetBackgroundColor(LtGrey)

	generatedReturnAddenda.FindAddendaFieldByName(TraceNo).SetValue(returnTraceNo).SetBackgroundColor(LtGrey)
	generatedReturnEntry.FindEntryFieldByName(TraceNo).SetValue(returnTraceNo).SetBackgroundColor(LtGrey)

	batchHeaderField, batchControlField := generatedReturnBatch.GetBatchFieldByName(OriginatingDFI)
	batchHeaderField.SetValue(originalReceiver).SetBackgroundColor(LtGrey)
	batchControlField.SetValue(originalReceiver).SetBackgroundColor(LtGrey)

	batchNoHeader, batchNoControl := generatedReturnBatch.GetBatchFieldByName(BatchNo)
	batchNoHeader.SetValue(returnBatchNo).SetBackgroundColor(LtGrey)
	batchNoControl.SetValue(returnBatchNo).SetBackgroundColor(LtGrey)

	batchCompanyIDHeader, batchCompanyIDControl := generatedReturnBatch.GetBatchFieldByName(CompanyID)
	batchCompanyIDHeader.SetValue(returnBatchCompanyID).SetBackgroundColor(LtGrey)
	batchCompanyIDControl.SetValue(returnBatchCompanyID).SetBackgroundColor(LtGrey)

	// Create Random Company Name and Description
	seed := time.Now().UTC().UnixNano()
	nameGenerator := namegenerator.NewNameGenerator(seed)
	name := nameGenerator.Generate()
	companyName := strings.Title(strings.Split(name, "-")[0]) + " LLC"

	batchCompanyName, _ := generatedReturnBatch.GetBatchFieldByName(CompanyName)
	batchCompanyName.SetValue(companyName).SetBackgroundColor(LtGrey)

	var totalEntryAddendaCount int
	for _, batch := range toFile.File.Batches {
		var batchEntryAddendaCount int
		for _, entry := range batch.Entries {
			totalEntryAddendaCount++
			batchEntryAddendaCount++
			if entry.Addenda99 != nil {
				totalEntryAddendaCount++
				batchEntryAddendaCount++
			}
		}
		batchEACountString := strconv.Itoa(batchEntryAddendaCount)
		_, batchEntryAddendaField := batch.GetBatchFieldByName(EntryAddendaCount)
		batchEntryAddendaField.SetValue(batchEACountString).SetBackgroundColor(LtGrey)
	}
	fileEACountString := strconv.Itoa(totalEntryAddendaCount)
	toFile.File.GetFileFieldByName(EntryAddendaCount).SetValue(fileEACountString).SetBackgroundColor(LtGrey)
}
