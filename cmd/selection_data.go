package cmd

type SelectionData struct {
	LineNumber      int
	SelectionNo     int
	LastSelectionNo int
}

func (sd *SelectionData) UpdateSelectionNo(number int) {
	sd.LastSelectionNo = sd.SelectionNo
	sd.SelectionNo = number
}

func (sd *SelectionData) GetLineNo() int {
	return sd.LineNumber
}

func (sd *SelectionData) SetLineNo(number int) {
	sd.LineNumber = number
}

func (sd *SelectionData) GetSelectionNo() int {
	return sd.SelectionNo
}

func (sd *SelectionData) DeepCopy() SelectionData {
	return SelectionData{}
}
