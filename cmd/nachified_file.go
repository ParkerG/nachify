package cmd

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/atotto/clipboard"
	"github.com/gdamore/tcell/v2"
	"github.com/pkg/errors"
	"github.com/rivo/tview"
)

type NachifiedFile struct {
	ReturnFile                bool
	ParentController          *NachifyController
	File                      *File
	View                      *tview.TextView
	SelectionIndex            int
	AlternateNachifyFile      *NachifiedFile
	AlternateView             *tview.TextView
	DefinitionView            *tview.TextView
	LegendView                *tview.TextView
	TipView                   *tview.TextView
	EditorView                *tview.InputField
	LineNumberToNachifyRecord map[int][]NachifyField
	LineCounter               int
	SelectionIDToNachifyField map[int]NachifyField
	SelectionIDCounter        int
	LabelMaker                []RecordLabel
	Legends                   []string
	CombinedLegend            string
}

func NewNachifiedFile(file *File) *NachifiedFile {
	nachifiedFile := NachifiedFile{
		SelectionIDToNachifyField: map[int]NachifyField{},
		SelectionIDCounter:        1,
		LineCounter:               0,
	}

	labelMaker = []RecordLabel{}
	lineNumberToNachifyRecord := map[int][]NachifyField{}
	nachifyLegend = []string{}
	var nachifyGroup []NachifyField
	file.FileHeaderLine = nachifiedFile.LineCounter
	for _, fhf := range file.FileHeaderFields {
		// Set line number data for navigation purposes
		fhf.SetLineNo(nachifiedFile.LineCounter)
		nachifyGroup = append(nachifyGroup, fhf)

		// Set selection tags for file header fields
		nachifiedFile.NewNachifySelection(fhf)
	}

	labelMaker = append(labelMaker, FileHeaderLabel)
	nachifyLegend = nachifyLegend.NewLegendEntry("1 " + GetLabelByLegend(FileHeaderLabel))

	// Map the line number to the group of fields within the file control record
	lineNumberToNachifyRecord[nachifiedFile.LineCounter] = nachifyGroup
	nachifiedFile.LineCounter++
	// Clear the slice
	nachifyGroup = []NachifyField{}

	batchPositionCounter := 0
	for _, batch := range file.Batches {
		batch.FilePosition = batchPositionCounter
		batch.BatchHeaderLine = nachifiedFile.LineCounter
		batchPositionCounter++
		for _, bhf := range batch.BatchHeaderFields {
			// Set line number data for navigation purposes
			bhf.SetLineNo(nachifiedFile.LineCounter)
			nachifyGroup = append(nachifyGroup, bhf)

			// Set selection tags for batch header fields
			nachifiedFile.NewNachifySelection(bhf)
		}

		labelMaker = append(labelMaker, BatchHeaderLabel)
		nachifyLegend = nachifyLegend.NewLegendEntry("5 " + GetLabelByLegend(BatchHeaderLabel))

		// Map the line number to the group of fields within the batch header record
		lineNumberToNachifyRecord[nachifiedFile.LineCounter] = nachifyGroup
		nachifiedFile.LineCounter++
		// Clear the slice
		nachifyGroup = []NachifyField{}

		entryPositionCounter := 0
		for _, entry := range batch.Entries {
			entry.LineNumber = nachifiedFile.LineCounter
			entry.BatchPosition = entryPositionCounter
			entryPositionCounter++
			for _, ef := range entry.EntryFields {
				// Set line number data for navigation purposes
				ef.SetLineNo(nachifiedFile.LineCounter)
				nachifyGroup = append(nachifyGroup, ef)

				// Set selection tags for entry fields
				nachifiedFile.NewNachifySelection(ef)
			}

			labelMaker = append(labelMaker, EntryLabel)
			nachifyLegend = nachifyLegend.NewLegendEntry("6 " + GetLabelByLegend(EntryLabel))

			// Map the line number to the group of fields within the entry record
			lineNumberToNachifyRecord[nachifiedFile.LineCounter] = nachifyGroup
			nachifiedFile.LineCounter++
			// Clear the slice
			nachifyGroup = []NachifyField{}

			if entry.Addenda99 != nil {
				for _, af := range entry.Addenda99.AddendaFields {
					// Set line number data for navigation purposes
					af.SetLineNo(nachifiedFile.LineCounter)
					nachifyGroup = append(nachifyGroup, af)

					// Set selection tag for addenda fields
					nachifiedFile.NewNachifySelection(af)
				}

				labelMaker = append(labelMaker, AddendaLabel)
				nachifyLegend = nachifyLegend.NewLegendEntry("7 " + (GetLabelByLegend(AddendaLabel)))

				// Map the line number to the group of fields within the addenda record
				lineNumberToNachifyRecord[nachifiedFile.LineCounter] = nachifyGroup
				nachifiedFile.LineCounter++
				// Clear the slice
				nachifyGroup = []NachifyField{}
			}
		}
		batch.BatchControlLine = nachifiedFile.LineCounter
		for _, bcf := range batch.BatchControlFields {
			// Set line number data for navigation purposes
			bcf.SetLineNo(nachifiedFile.LineCounter)
			nachifyGroup = append(nachifyGroup, bcf)

			// Set selection tag for batch control fields
			nachifiedFile.NewNachifySelection(bcf)
		}

		labelMaker = append(labelMaker, BatchControlLabel)
		nachifyLegend = nachifyLegend.NewLegendEntry("8 " + GetLabelByLegend(BatchControlLabel))

		// Map the line number to the group of fields within the batch control record
		lineNumberToNachifyRecord[nachifiedFile.LineCounter] = nachifyGroup
		nachifiedFile.LineCounter++
		// Clear the slice
		nachifyGroup = []NachifyField{}
	}
	file.FileControlLine = nachifiedFile.LineCounter
	for _, fcf := range file.FileControlFields {
		// Set line number data for navigation purposes
		fcf.SetLineNo(nachifiedFile.LineCounter)
		nachifyGroup = append(nachifyGroup, fcf)

		// Set selection tag for batch control fields
		nachifiedFile.NewNachifySelection(fcf)
	}

	labelMaker = append(labelMaker, FileControlLabel)
	nachifyLegend = nachifyLegend.NewLegendEntry("9 " + GetLabelByLegend(FileControlLabel))

	nachifiedFile.CombinedLegend = strings.Join(nachifyLegend, "\n")
	// Map the line number to the group of fields within the file control record
	lineNumberToNachifyRecord[nachifiedFile.LineCounter] = nachifyGroup

	nachifiedFile.File = file
	nachifiedFile.LinkFields()
	nachifiedFile.LineNumberToNachifyRecord = lineNumberToNachifyRecord
	nachifiedFile.LabelMaker = labelMaker

	return &nachifiedFile
}

func (nf *NachifiedFile) SetController(nc *NachifyController) {
	nf.ParentController = nc
}

func (nf *NachifiedFile) SetPrimaryView(view *tview.TextView) {
	nf.View = view
	document := nf.NachifyFileWithStyle()
	fmt.Fprintf(nf.View, "%s", document)
}

func (nf *NachifiedFile) GetPrimaryView() *tview.TextView {
	return nf.View
}

func (nf *NachifiedFile) SetAlternateView(view *tview.TextView) {
	nf.AlternateView = view
}

func (nf *NachifiedFile) GetAlternateView() *tview.TextView {
	return nf.AlternateView
}

func (nf *NachifiedFile) SetDefinitionView(view *tview.TextView) {
	nf.DefinitionView = view
	fmt.Fprintf(nf.DefinitionView, "%s\n\n\nBatches: %d\nEntries: %d\nReturn Entries: %d", "To get started press [green]ENTER[white] to focus on the file then use the [blue]DIRECTIONAL ARROWS[white] to cycle through the fields", nf.File.TotalBatchCount, nf.File.TotalEntryCount, nf.File.TotalReturnEntries)
}

func (nf *NachifiedFile) ResetDefinitionView(selectedField NachifyField) {
	definition := FormatDefinition(selectedField.GetRecordPrefix(), selectedField.GetPosition(), selectedField.GetDefinition(), selectedField.GetLength())
	viewHeader := ColorText(selectedField.GetName(), selectedField.GetTextColor())
	nf.DefinitionView.Clear()
	fmt.Fprintf(nf.DefinitionView, "%s", definition)
	nf.DefinitionView.SetTitle(" [::b]" + viewHeader + "[::-] [Ctrl + L] ")
	nf.DefinitionView.ScrollToBeginning()
}

func (nf *NachifiedFile) GetDefinitionView() *tview.TextView {
	return nf.DefinitionView
}

func (nf *NachifiedFile) GetLegend() string {
	return nf.CombinedLegend
}

//NachifyFileWithStyle applies styling to all the fields by iterating over
// LineNumberToNachifyRecord. It also attaches labels to the end of lines
func (nf *NachifiedFile) NachifyFileWithStyle() string {
	var nachifiedLines []string

	for i := 0; i < len(nf.LineNumberToNachifyRecord); i++ {
		nachifiedLine := ""
		for _, nachifyField := range nf.LineNumberToNachifyRecord[i] {
			nachifiedLine += nachifyField.RenderStyle()
		}
		nachifiedLines = append(nachifiedLines, nachifiedLine)
	}

	nachifiedLines = nf.ApplyLabels(nachifiedLines)

	// Complete the block of 10
	for len(nachifiedLines)%10 != 0 {
		nachifiedLines = append(nachifiedLines, strings.Repeat("9", 94))
	}
	return strings.Join(nachifiedLines, "\n")
}

func (nf *NachifiedFile) NewNachifySelection(field NachifyField) {
	nf.SelectionIDToNachifyField[nf.SelectionIDCounter] = field
	field.UpdateSelectionNo(nf.SelectionIDCounter)
	nf.SelectionIDCounter++
}

func (nf *NachifiedFile) ResetView(content string) {
	nf.View.Clear()
	fmt.Fprintf(nf.View, "%s", content)
}

func (nf *NachifiedFile) ViewInputHandler(event *tcell.EventKey) {
	highLighted := nf.View.GetHighlights()
	if event.Key() == tcell.KeyEnter {
		if len(highLighted) > 0 {
			nf.View.Highlight()
		} else {
			nf.SelectionIndex = 1
			selectedField := nf.GetFieldByID(nf.SelectionIndex)
			nf.View.Highlight("1").ScrollToHighlight()
			go nf.ResetDefinitionView(selectedField)
		}
	} else if len(highLighted) > 0 {
		switch event.Key() {
		case tcell.KeyUp:
			if nf.View.HasFocus() && len(nf.View.GetHighlights()) > 0 {
				nf.MoveUp()
			}
		case tcell.KeyDown:
			if nf.View.HasFocus() && len(nf.View.GetHighlights()) > 0 {
				nf.MoveDown()
			}
		case tcell.KeyLeft:
			if nf.View.HasFocus() && len(nf.View.GetHighlights()) > 0 {
				nf.MoveLeft()
			}
		case tcell.KeyRight:
			if nf.View.HasFocus() && len(nf.View.GetHighlights()) > 0 {
				nf.MoveRight()
			}
		case tcell.KeyCtrlD: // Copy selected field
			if nf.View.HasFocus() && len(nf.View.GetHighlights()) > 0 {
				initialField = nf.GetSelectedField()
			}
		case tcell.KeyCtrlG: // Generate new file
			//nf.SaveNewFile("")
			if showingReturnView {
				go ShowSaveFileWithReturnView(nf)
			} else {
				go ShowSaveFileView(nf)
			}
		case tcell.KeyCtrlA: // Add addenda99
			selectedField := nf.GetSelectedField()
			AddAddenda99(selectedField)
		case tcell.KeyCtrlL:
			app.SetFocus(nf.DefinitionView)
		case tcell.KeyCtrlS: // Switch copied field with selected field
			if initialField != nil && nf.View.HasFocus() && len(nf.View.GetHighlights()) > 0 {
				nf.SwitchText(initialField, nf.GetSelectedField())
				updatedDoc := nf.NachifyFileWithStyle()
				nf.ResetView(updatedDoc)
			}
		case tcell.KeyCtrlV: // Paste selected field
			if initialField != nil && nf.View.HasFocus() && len(nf.View.GetHighlights()) > 0 {
				nf.CopyText(initialField, nf.GetSelectedField())
				updatedDoc := nf.NachifyFileWithStyle()
				nf.ResetView(updatedDoc)
			}
		case tcell.KeyCtrlN: // TODO: Incomplete
			selectedField := nf.GetSelectedField()
			if selectedEntry, ok := selectedField.(*EntryField); ok {
				_ = selectedEntry.NewFileWithSingleEntry()
			}
		case tcell.KeyCtrlE: // Edit selected field
			if showingReturnView {
				go ShowEditorWithReturnView(nf)
			} else {
				go ShowEditorView(nf)
			}
		case tcell.KeyCtrlRightSq: // Copy selected field to clipboard
			selectedText := nf.GetSelectedField().GetTextValue()
			clipboard.WriteAll(strings.TrimSpace(selectedText))
			// TODO: Refactor these to functions
		case tcell.KeyPgUp: // Increment value of selected field
			{
				selectedField := nf.GetSelectedField()
				if selectedEntry, ok := selectedField.(*EntryField); ok {
					fieldName := selectedEntry.GetName()
					if fieldName == TraceNo {
						currentValue := selectedEntry.GetIntValue()
						newValue := currentValue + 1
						selectedEntry.SetValue(strconv.Itoa(newValue))
						selectedEntry.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
					if fieldName == Amount {
						currentValue := selectedEntry.GetIntValue()
						newValue := currentValue + 250
						selectedEntry.SetValue(strconv.Itoa(newValue))
						selectedEntry.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
					if fieldName == TransactionCode {
						selectedEntry.ParentEntry.ReassignDCLinks("22")
						selectedEntry.SetValue("22")
						selectedEntry.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
				} else if selectedFileField, ok := selectedField.(*FileField); ok {
					fieldName := selectedFileField.GetName()
					if fieldName == FileCreationDate {
						v := selectedFileField.GetTextValue()
						newValue := IncrementDate(v)
						selectedFileField.SetValue(newValue)
						selectedFileField.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
				} else if selectedBatchField, ok := selectedField.(*BatchField); ok {
					fieldName := selectedBatchField.GetName()
					if fieldName == CompanyDescriptiveDate || fieldName == EffectiveEntryDate {
						v := selectedBatchField.GetTextValue()
						newValue := IncrementDate(v)
						selectedBatchField.SetValue(newValue)
						selectedBatchField.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
				}
			}
		// TODO: Refactor these to functions
		case tcell.KeyPgDn: // Decrement value of selected field
			{
				selectedField := nf.GetSelectedField()
				if selectedEntry, ok := selectedField.(*EntryField); ok {
					fieldName := selectedEntry.GetName()
					if fieldName == TraceNo {
						currentValue := selectedEntry.GetIntValue()
						newValue := currentValue - 1
						selectedEntry.SetValue(strconv.Itoa(newValue))
						selectedEntry.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
					if fieldName == Amount {
						currentValue := selectedEntry.GetIntValue()
						newValue := currentValue - 250
						selectedEntry.SetValue(strconv.Itoa(newValue))
						selectedEntry.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
					if fieldName == TransactionCode {
						selectedEntry.ParentEntry.ReassignDCLinks("27")
						selectedEntry.SetValue("27")
						selectedEntry.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
				} else if selectedFileField, ok := selectedField.(*FileField); ok {
					fieldName := selectedFileField.GetName()
					if fieldName == FileCreationDate {
						v := selectedFileField.GetTextValue()
						newValue := DecrementDate(v)
						selectedFileField.SetValue(newValue)
						selectedFileField.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
				} else if selectedBatchField, ok := selectedField.(*BatchField); ok {
					fieldName := selectedBatchField.GetName()
					if fieldName == CompanyDescriptiveDate || fieldName == EffectiveEntryDate {
						v := selectedBatchField.GetTextValue()
						newValue := DecrementDate(v)
						selectedBatchField.SetValue(newValue)
						selectedBatchField.SetBackgroundColor(LtGrey)
						updatedDoc := nf.NachifyFileWithStyle()
						nf.ResetView(updatedDoc)
					}
				}
			}
		case tcell.KeyCtrlY:
			if nf.AlternateNachifyFile == nil {
				selectedField := nf.GetSelectedField()
				if selectedEntry, ok := selectedField.(*EntryField); ok {
					go nf.CreateReturnFromEntry(selectedEntry)
				}
			} else {
				alternate := nf.AlternateNachifyFile
				controller.SetState(alternate)
			}
		}
	}
}

func (nf *NachifiedFile) IsReturnFile() bool {
	return nf.ReturnFile
}

func (nf *NachifiedFile) CreateReturnFromEntry(ef *EntryField) {
	entryLineNo := ef.LineNumber
	batch := ef.ParentEntry.Batch
	file := batch.File
	batchHeaderLinNo := batch.BatchHeaderLine
	batchControlLinNo := batch.BatchControlLine
	fileHeaderLineNo := file.FileHeaderLine
	fileControlLineNo := file.FileControlLine

	var newLines []string

	newLines = append(newLines, controller.GetStringRecordByLine(fileHeaderLineNo))
	newLines = append(newLines, controller.GetStringRecordByLine(batchHeaderLinNo))
	newLines = append(newLines, controller.GetStringRecordByLine(entryLineNo))
	newLines = append(newLines, controller.GetStringRecordByLine(batchControlLinNo))
	newLines = append(newLines, controller.GetStringRecordByLine(fileControlLineNo))
	document := strings.Join(newLines, "\n")
	reader := strings.NewReader(document)
	s := bufio.NewScanner(reader)
	returnFile := NewFile(s)

	// Add addenda to the entry
	newEntry := returnFile.Batches[0].Entries[0].EntryFields[0]
	AddAddenda99(newEntry)

	returnView = NewReturnView(app, " RETURN [Ctrl + R] ")

	// Create screen view for return
	flex.Clear()
	flexReturn := NewFlexView(fileView, returnView, definitionView)
	app.SetRoot(flexReturn, true).SetFocus(returnView)
	app.Draw()
	showingReturnView = true

	rnf := NewNachifiedFile(returnFile)
	RunReturnEntryAssistant(ef.ParentEntry, rnf)

	rnf.SetPrimaryView(returnView)
	rnf.ReturnFile = true
	rnf.SetDefinitionView(nf.DefinitionView)

	nf.AlternateView = rnf.View
	rnf.AlternateView = nf.View

	// Set controller to control new view
	rnf.SetController(nf.ParentController)
	controller.SetState(rnf)

	// Set alternates so we can switch back and forth
	rnf.AlternateNachifyFile = nf
	nf.AlternateNachifyFile = rnf
}

func InsertNewEntry(ef *EntryField) *File {
	entryLineNo := ef.LineNumber
	batch := ef.ParentEntry.Batch
	file := batch.File

	var newLines []string

	newLines = append(newLines, controller.GetStringRecordByLine(file.FileHeaderLine))

	for _, fileBatch := range file.Batches {
		newLines = append(newLines, controller.GetStringRecordByLine(fileBatch.BatchHeaderLine))
		for _, fileEntry := range fileBatch.Entries {
			newLine := controller.GetStringRecordByLine(fileEntry.LineNumber)
			newLines = append(newLines, newLine)
			var addendaLine string
			if fileEntry.IsReturn() {
				addendaLine = controller.GetStringRecordByLine(fileEntry.LineNumber + 1)
				newLines = append(newLines, addendaLine)
			}

			if fileEntry.LineNumber == entryLineNo {
				newLines = append(newLines, newLine)
				if fileEntry.IsReturn() {
					newLines = append(newLines, addendaLine)
				}
			}
		}
		newLines = append(newLines, controller.GetStringRecordByLine(fileBatch.BatchControlLine))
	}
	newLines = append(newLines, controller.GetStringRecordByLine(file.FileControlLine))

	document := strings.Join(newLines, "\n")
	reader := strings.NewReader(document)
	s := bufio.NewScanner(reader)
	return NewFile(s)
}

func RemoveEntry(ef *EntryField) *File {
	entryLineNo := ef.LineNumber
	batch := ef.ParentEntry.Batch
	file := batch.File

	var newLines []string

	newLines = append(newLines, controller.GetStringRecordByLine(file.FileHeaderLine))

	for _, fileBatch := range file.Batches {
		newLines = append(newLines, controller.GetStringRecordByLine(fileBatch.BatchHeaderLine))
		batchEntryCount := 0
		for _, fileEntry := range fileBatch.Entries {
			if fileEntry.LineNumber != entryLineNo {
				batchEntryCount++
				newLine := controller.GetStringRecordByLine(fileEntry.LineNumber)
				newLines = append(newLines, newLine)
				var addendaLine string
				if fileEntry.IsReturn() {
					addendaLine = controller.GetStringRecordByLine(fileEntry.LineNumber + 1)
					newLines = append(newLines, addendaLine)
				}
			}
		}
		// If we have removed the only entry in the batch, remove the batch completely
		if batchEntryCount == 0 {
			newLines = newLines[:len(newLines)-1]
			continue
		}
		newLines = append(newLines, controller.GetStringRecordByLine(fileBatch.BatchControlLine))
	}
	newLines = append(newLines, controller.GetStringRecordByLine(file.FileControlLine))

	document := strings.Join(newLines, "\n")
	reader := strings.NewReader(document)
	s := bufio.NewScanner(reader)
	return NewFile(s)
}

func (nf *NachifiedFile) ApplyLabels(lines []string) []string {
	for i := range lines {
		lines[i] += " " + string(nf.LabelMaker[i])
	}
	return lines
}

func (nf *NachifiedFile) LinkFields() {
	// Link Credit, Debit and Hash Fields
	fileCreditField := nf.File.GetFileFieldByName(TotalFileCreditAmount)
	fileDebitField := nf.File.GetFileFieldByName(TotalFileDebitAmount)
	fileHashField := nf.File.GetFileFieldByName(EntryHash)

	fileEntryAddendaCount := 0
	for _, batch := range nf.File.Batches {
		batchEntryAddendaCount := 0
		_, batchCreditField := batch.GetBatchFieldByName(TotalCreditAmount)
		_, batchDebitField := batch.GetBatchFieldByName(TotalDebitAmount)
		_, batchHashField := batch.GetBatchFieldByName(EntryHash)
		batchHeaderCompanyID, batchControlCompanyID := batch.GetBatchFieldByName(CompanyID)
		batchHeaderODFI, batchControlODFI := batch.GetBatchFieldByName(OriginatingDFI)
		batchHeaderSCC, batchControlSCC := batch.GetBatchFieldByName(ServiceClassCode)
		batchHeaderNo, batchControlNo := batch.GetBatchFieldByName(BatchNo)
		LinkMirroredFields(batchHeaderCompanyID, batchControlCompanyID)
		LinkMirroredFields(batchHeaderODFI, batchControlODFI)
		LinkMirroredFields(batchHeaderSCC, batchControlSCC)
		LinkMirroredFields(batchHeaderNo, batchControlNo)
		for _, entry := range batch.Entries {
			batchEntryAddendaCount++
			if entry.IsReturn() {
				batchEntryAddendaCount++
			}
			entryAmountField := entry.FindEntryFieldByName(Amount)
			entryRDFIField := entry.FindEntryFieldByName(RdfiRoutingNo)
			if entry.DCSign() == CREDIT {
				LinkAggregates(batchCreditField, entryAmountField)
			} else if entry.DCSign() == DEBIT {
				LinkAggregates(batchDebitField, entryAmountField)
			}
			LinkAggregates(batchHashField, entryRDFIField)
		}
		fileEntryAddendaCount += batchEntryAddendaCount
		_, bcFieldEntryAddendaCount := batch.GetBatchFieldByName(EntryAddendaCount)
		val := strconv.Itoa(batchEntryAddendaCount)
		bcFieldEntryAddendaCount.SetValue(val)
		LinkAggregates(fileCreditField, batchCreditField)
		LinkAggregates(fileDebitField, batchDebitField)
		LinkAggregates(fileHashField, batchHashField)
	}
	val := strconv.Itoa(fileEntryAddendaCount)
	fileFieldEntryAddendaCount := nf.File.GetFileFieldByName(EntryAddendaCount)
	fileFieldEntryAddendaCount.SetValue(val)
}

func (nf *NachifiedFile) UpdateCounterFields() {
	for _, batch := range nf.File.Batches {
		_, bcEntryAddendaCount := batch.GetBatchFieldByName(EntryAddendaCount)

		val := strconv.Itoa(len(batch.Entries))
		bcEntryAddendaCount.SetValue(val)
	}
}

func (nf *NachifiedFile) MoveUp() {
	selectedField := nf.GetSelectedField()
	currentLineNo := selectedField.GetLineNo()
	if currentLineNo == 0 {
		return
	}
	previousLineSelectables := nf.LineNumberToNachifyRecord[currentLineNo-1]
	for _, s := range previousLineSelectables {
		if s.GetLastPosition() >= selectedField.GetStartPosition() {
			nf.SelectionIndex = s.GetSelectionNo()
			newSelectedField := nf.GetFieldByID(nf.SelectionIndex)
			nf.View.Highlight(strconv.Itoa(nf.SelectionIndex)).ScrollToHighlight()
			go nf.ResetDefinitionView(newSelectedField)
			return
		}
	}
}

func (nf *NachifiedFile) MoveDown() {
	currentSelectable := nf.GetSelectedField()
	currentLineNo := currentSelectable.GetLineNo()
	if currentLineNo == nf.LineCounter {
		return
	}
	nextLineSelectables := nf.LineNumberToNachifyRecord[currentLineNo+1]
	for _, targetSelectable := range nextLineSelectables {
		if targetSelectable.GetStartPosition() > currentSelectable.GetStartPosition() || targetSelectable.GetLastPosition() > targetSelectable.GetStartPosition() || targetSelectable.GetLastPosition() >= currentSelectable.GetLastPosition() {
			nf.SelectionIndex = targetSelectable.GetSelectionNo()
			newSelectedField := nf.GetFieldByID(nf.SelectionIndex)
			nf.View.Highlight(strconv.Itoa(nf.SelectionIndex)).ScrollToHighlight()
			go nf.ResetDefinitionView(newSelectedField)
			return
		}
	}
}

func (nf *NachifiedFile) MoveLeft() {
	nf.SelectionIndex = (nf.SelectionIndex - 1 + nf.SelectionIDCounter) % nf.SelectionIDCounter
	if nf.SelectionIndex == 0 {
		nf.SelectionIndex = 1
	}
	selectedField := nf.GetFieldByID(nf.SelectionIndex)
	nf.View.Highlight(strconv.Itoa(nf.SelectionIndex)).ScrollToHighlight()
	go nf.ResetDefinitionView(selectedField)
}

func (nf *NachifiedFile) MoveRight() {
	nf.SelectionIndex = (nf.SelectionIndex + 1) % nf.SelectionIDCounter
	if nf.SelectionIndex == 0 {
		nf.SelectionIndex = 1
	}
	selectedField := nf.GetFieldByID(nf.SelectionIndex)
	nf.View.Highlight(strconv.Itoa(nf.SelectionIndex)).ScrollToHighlight()
	go nf.ResetDefinitionView(selectedField)
}

func (nf *NachifiedFile) GetSelectedField() NachifyField {
	return nf.SelectionIDToNachifyField[nf.SelectionIndex]
}

func (nf *NachifiedFile) GetFieldByID(id int) NachifyField {
	return nf.SelectionIDToNachifyField[id]
}

// GetStringRecordByLine returns the plain text of the line
func (nf *NachifiedFile) GetStringRecordByLine(lineNo int) string {
	fields := nf.LineNumberToNachifyRecord[lineNo]
	var stringRecord string
	for _, field := range fields {
		stringRecord += field.RenderPlain()
	}
	return stringRecord
}

func (nf *NachifiedFile) SwitchText(selectableOne, selectableTwo NachifyField) {
	textOne := selectableOne.GetTextValue()
	textTwo := selectableTwo.GetTextValue()

	if selectableOne.GetLength() == selectableTwo.GetLength() {
		selectableOne.SetValue(textTwo)
		selectableOne.SetBackgroundColor(LtGrey)
		selectableTwo.SetValue(textOne)
		selectableTwo.SetBackgroundColor(LtGrey)
	}
}

func (nf *NachifiedFile) CopyText(fromField, toField NachifyField) {
	fromText := fromField.GetTextValue()

	if fromField.GetLength() == toField.GetLength() {
		toField.SetValue(fromText)
		toField.SetBackgroundColor(LtGrey)
	}
}

func (nf *NachifiedFile) GetPlainFile() string {
	var fileLines []string
	for i := 0; i < len(nf.LineNumberToNachifyRecord); i++ {
		nachifiedLine := ""
		for _, nachifyField := range nf.LineNumberToNachifyRecord[i] {
			nachifiedLine += nachifyField.RenderPlain()
		}
		fileLines = append(fileLines, nachifiedLine)
	}

	// Complete the block of 10
	for len(fileLines)%10 != 0 {
		fileLines = append(fileLines, strings.Repeat("9", 94))
	}
	return strings.Join(fileLines, "\n")
}

func (nf *NachifiedFile) SaveNewFile(name string) (string, error) {
	var fileLines []string
	for i := 0; i < len(nf.LineNumberToNachifyRecord); i++ {
		nachifiedLine := ""
		for _, nachifyField := range nf.LineNumberToNachifyRecord[i] {
			nachifiedLine += nachifyField.RenderPlain()
		}
		fileLines = append(fileLines, nachifiedLine)
	}

	// Complete the block of 10
	for len(fileLines)%10 != 0 {
		fileLines = append(fileLines, strings.Repeat("9", 94))
	}
	document := strings.Join(fileLines, "\n")
	if name == "#" {
		_ = clipboard.WriteAll(document)
		return "", nil
	}

	var fullPath string
	homeDir, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	genDirectory := path.Join(homeDir, "Generated_ACHs")
	_, err = os.Stat(genDirectory)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(genDirectory, 0755)
		if errDir != nil {
			log.Fatal(err)
		}
	}

	filenameSuffix := 1
	var fileName string
	for {
		if name == "" {
			fileName = fmt.Sprintf("gen_%d.ach", filenameSuffix)
		} else {
			fileName = name
		}
		fullPath = filepath.Join(genDirectory, fileName)
		_, err := os.Stat(fullPath)
		if os.IsNotExist(err) {
			file, err := os.Create(fullPath)
			if err != nil {
				log.Fatal(err)
			}

			_, err = file.WriteString(document)
			if err != nil {
				log.Fatal(err)
			}
			break
		} else if err != nil {
			log.Fatal(err)
		} else if name == "" {
			filenameSuffix++
		} else if name == "return_"+inputFileName {
			name = ""
		} else {
			return "", errors.Errorf("File %s already exist!", name)
		}
	}
	return fullPath, nil
}
