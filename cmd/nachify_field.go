package cmd

var PrimaryFile *File
var ReturnFile *File

type LabelMaker []RecordLabel

var labelMaker LabelMaker

type legend []string

var nachifyLegend legend

type RecordLabel string

const (
	FileHeaderLabel   RecordLabel = "FileHeader"
	BatchHeaderLabel  RecordLabel = "BatchHeader"
	EntryLabel        RecordLabel = "Entry"
	AddendaLabel      RecordLabel = "Addenda"
	BatchControlLabel RecordLabel = "BatchControl"
	FileControlLabel  RecordLabel = "FileControl"
)

var PrimaryLineNoToNachifyGroup = map[int][]NachifyField{}
var primaryFileLineCount int

var ReturnLineNoToNachifyGroup = map[int][]NachifyField{}
var returnFileLineCount int

var SelectionIDToNachifyField = map[int]NachifyField{} // This is specific to the fileview
var nachifyFieldCount int                              // Incremented each time nachify field is created.

// NachifyField exposes the methods to get and update the values for a specific field
type NachifyField interface {

	// SELECTION METHODS
	UpdateSelectionNo(number int)
	GetSelectionNo() int

	// STYLE METHODS
	Bold() *StyleData
	Underline() *StyleData
	SetBackgroundColor(color string) *StyleData
	RenderStyle() string
	RenderPlain() string

	// FIELD METHODS
	GetFieldData() *FieldData
	SetLineNo(number int)
	SetValue(value string) NachifyField
	MirrorField(field NachifyField)
	RecalculateMirroredField()
	RecalculateValue()
	AddAggregatePart(part NachifyField)
	RemoveAggregatePart(part NachifyField)
	InsertAggregator(part NachifyField)
	RemoveAggregator(part NachifyField)
	GetAggregators() []NachifyField
	GetAggregateParts() []NachifyField
	GetTextValue() string
	GetIntValue() int
	GetRecordPrefix() string
	GetName() string
	GetContents() string
	GetDefinition() string
	GetTextColor() string
	GetPosition() string
	GetStartPosition() int
	GetLastPosition() int
	GetLineNo() int
	GetLength() int
}

func (l legend) NewLegendEntry(text string) legend {
	last := len(l) - 1
	if len(l) == 0 || l[last] != text {
		l = append(l, text)
	}
	return l
}

// UnlinkAggregates dis-associates an aggregator, a field whose value is the sum of other fields, from
// the aggregates, the fields having a value that contribute to the sum total of the aggregator value.
func UnlinkAggregates(aggregator NachifyField, aggregatePart NachifyField) {
	aggregator.RemoveAggregatePart(aggregatePart)
	aggregatePart.RemoveAggregator(aggregator)
	aggregator.RecalculateValue()

	ags := aggregatePart.GetAggregators()
	ags = append(ags, aggregator)
	DoRecursiveRecalculation(ags)
}

//LinkAggregates associates an aggregator, a field whose value is the sum of other fields, with
// the aggregates, the fields having a value that contribute to the sum total of the aggregator value.
func LinkAggregates(aggregator NachifyField, aggregatePart NachifyField) {
	aggregator.AddAggregatePart(aggregatePart)
	aggregatePart.InsertAggregator(aggregator)
	aggregator.RecalculateValue()

	ags := aggregatePart.GetAggregators()
	DoRecursiveRecalculation(ags)
}

//LinkMirroredFields creates an association between 2 fields so should share the same value.
// One example is the ODFI field in the batch header and the ODFI in the batch control
func LinkMirroredFields(fieldOne, fieldTwo NachifyField) {
	fieldOne.MirrorField(fieldTwo)
	fieldTwo.MirrorField(fieldOne)
}
