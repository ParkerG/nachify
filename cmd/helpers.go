package cmd

import (
	"bufio"
	"fmt"

	"github.com/enescakir/emoji"
	"github.com/pkg/errors"

	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

type RecordType string

var randomTraceNumber int
var randomBatchNumber int
var randomCompanyID int

const (
	FileRecordHeader   RecordType = "1"
	BatchRecordHeader  RecordType = "5"
	EntryRecord        RecordType = "6"
	AddendaRecord      RecordType = "7"
	BatchRecordControl RecordType = "8"
	FileRecordControl  RecordType = "9"
)

func newSplitScanner(all string) (*bufio.Scanner, error) {
	var lines []string
	start := 0
	end := start + 94
	for end < len(all) {
		lines = append(lines, all[start:end])
		start = end
		end = start + 94
	}
	last := all[start:]
	if len(last) != 94 {
		return nil, errors.New("unable to parse ach file. invalid length")
	}
	lines = append(lines, last)
	text := strings.Join(lines, "\n")
	reader := strings.NewReader(text)
	return bufio.NewScanner(reader), nil
}

func ExtendWhiteSpace(line string, length int) string {
	l := length - len(line)
	if l >= 0 {
		spaces := strings.Repeat(" ", l)
		return line + spaces
	}
	return ""
}

func GetFieldNameAndDefinition(index int) (string, string) {
	selectable := SelectionIDToNachifyField[index]
	definition := FormatDefinition(selectable.GetRecordPrefix(), selectable.GetPosition(), selectable.GetDefinition(), selectable.GetLength())
	coloredFieldName := ColorText(selectable.GetName(), selectable.GetTextColor())
	return coloredFieldName, definition
}

func GetLabelByLegend(label RecordLabel) string {
	switch label {
	case FileHeaderLabel:
		return CreateFileHeaderLegend()
	case BatchHeaderLabel:
		return CreateBatchHeaderLegend()
	case EntryLabel:
		return CreateEntryLegend()
	case AddendaLabel:
		return CreateAddendaLegend()
	case BatchControlLabel:
		return CreateBatchControlLegend()
	case FileControlLabel:
		return CreateFileControlLegend()
	}
	return ""
}

func FormatDefinition(line string, position string, definition string, length int) string {
	lengthStr := strconv.Itoa(length)
	return fmt.Sprintf(
		`[::b]LINE:[::-] %s [::b]POSITION:[::-] %s
[::b]LENGTH:[::-] %s

[::bu]DEFINITION:[::-]
%s`, line, position, lengthStr, definition)
}

func GetTextValueFromPosition(original string, position string) (*string, *int) {
	indices := strings.Split(position, "-")
	startInt, _ := strconv.Atoi(indices[0])
	start := startInt - 1

	var end int
	if len(indices) == 1 {
		end = startInt
	} else {
		end, _ = strconv.Atoi(indices[1])
	}

	var subString string
	if end == 94 {
		subString = original[start:]
	} else {
		subString = original[start:end]
	}

	intString, err := strconv.Atoi(subString)
	if err != nil {
		return &subString, nil
	}
	return &subString, &intString
}

func LeftPaddedZeroes(length int, text string) string {
	return fmt.Sprintf("%0*s", length, text)
}

func RightPaddedSpaces(length int, text string) string {
	return fmt.Sprintf("%s%s", text, strings.Repeat(" ", length-len(text)))
}

func ValidateRoutingNumber(routingNo string) int {
	positionWeight := map[int]int{0: 3, 1: 7, 2: 1, 3: 3, 4: 7, 5: 1, 6: 3, 7: 7}
	var sum int
	for pos, mult := range positionWeight {
		num, err := strconv.Atoi(string(routingNo[pos]))
		if err != nil {
			return -1
		}
		// To compute check digit:
		// Multiply each digit in the routing number by the corresponding weighting factor and add the sum
		sum += num * mult
	}
	// Subtract the sum from the next highest multiple of 10. The result is the check digit.
	expected := (((sum / 10) + 1) * 10) - sum
	if expected == 10 {
		expected = 0
	}
	return expected
}

func NewRandomBatchNumber() string {
	max := 9999999
	min := 1000000
	if randomBatchNumber == 0 {
		value := rand.Intn(max-min) + min
		stringValue := strconv.Itoa(value)
		randomBatchNumber, _ = strconv.Atoi(stringValue)
		randomBatchNumber++
		return stringValue
	} else {
		newTraceNo := strconv.Itoa(randomBatchNumber)
		randomBatchNumber++
		return newTraceNo
	}
}

func NewRandomBatchCompanyID() string {
	max := 9999999999
	min := 1000000000
	if randomCompanyID == 0 {
		randomCompanyID = rand.Intn(max-min) + min
		stringValue := strconv.Itoa(randomCompanyID)
		return stringValue
	} else {
		doubled := randomCompanyID * 2
		stringValue := strconv.Itoa(doubled)
		stringValue = stringValue[0:10]
		randomCompanyID, _ = strconv.Atoi(stringValue)
		return stringValue
	}
}

func NewRandomTraceNumber(routingPrefix string) string {
	if len(routingPrefix) < 8 {
		return "TOOSHORT"
	}
	if len(routingPrefix) > 8 {
		return "TOOLONG!"
	}
	max := 9999999
	min := 1000000

	if randomTraceNumber == 0 || strconv.Itoa(randomTraceNumber)[0:9] != routingPrefix {
		value := rand.Intn(max-min) + min
		stringValue := strconv.Itoa(value)
		fullTraceNo := routingPrefix + stringValue
		randomTraceNumber, _ = strconv.Atoi(fullTraceNo)
		randomTraceNumber++
		return fullTraceNo
	} else {
		newTraceNo := strconv.Itoa(randomTraceNumber)
		randomTraceNumber++
		return newTraceNo
	}
}

func NewWarningMsg(msg string) string {
	return fmt.Sprintf("%s %s %s", emoji.Warning, msg, emoji.Warning)
}

func NewSuccessMsg(msg string) string {
	return fmt.Sprintf("%s %s %s", emoji.ThumbsUp, msg, emoji.ThumbsUp)
}

func IncrementDate(dateInput string) string {
	year := fmt.Sprintf("20%s", dateInput[0:2])
	day := dateInput[4:]
	month := dateInput[2:4]
	timeString := fmt.Sprintf("%s-%s-%sT00:00:00Z", year, month, day)
	result, _ := time.Parse(time.RFC3339, timeString)
	result = result.Add(time.Hour * 24)
	r := result.Format("060102")
	return r
}

func DecrementDate(dateInput string) string {
	year := fmt.Sprintf("20%s", dateInput[0:2])
	day := dateInput[4:]
	month := dateInput[2:4]
	timeString := fmt.Sprintf("%s-%s-%sT00:00:00Z", year, month, day)
	result, _ := time.Parse(time.RFC3339, timeString)
	result = result.Add(-time.Hour * 24)
	r := result.Format("060102")
	return r
}

func ExitOnErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
