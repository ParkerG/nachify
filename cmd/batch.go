package cmd

import (
	"fmt"
)

var BatchHeaderFieldContext = []FieldContext{
	FieldIDToContext["5_RecordTypeCode"],
	FieldIDToContext["5_ServiceClassCode"],
	FieldIDToContext["5_CompanyName"],
	FieldIDToContext["5_CompanyDiscretionaryData"],
	FieldIDToContext["5_CompanyID"],
	FieldIDToContext["5_StandardEntryClassCode"],
	FieldIDToContext["5_CompanyEntryDescription"],
	FieldIDToContext["5_CompanyDescriptiveDate"],
	FieldIDToContext["5_EffectiveEntryDate"],
	FieldIDToContext["5_SettlementDate"],
	FieldIDToContext["5_OriginatorStatusCode"],
	FieldIDToContext["5_OriginatingDFI"],
	FieldIDToContext["5_BatchNo"],
}

var BatchControlFieldContext = []FieldContext{
	FieldIDToContext["8_RecordTypeCode"],
	FieldIDToContext["8_ServiceClassCode"],
	FieldIDToContext["8_EntryAddendaCount"],
	FieldIDToContext["8_EntryHash"],
	FieldIDToContext["8_TotalDebitAmount"],
	FieldIDToContext["8_TotalCreditAmount"],
	FieldIDToContext["8_CompanyID"],
	FieldIDToContext["8_MessageAuthenticationCode"],
	FieldIDToContext["8_Reserved"],
	FieldIDToContext["8_OriginatingDFI"],
	FieldIDToContext["8_BatchNo"],
}

type Batch struct {
	File               *File
	FilePosition       int // Needed to perform new batch insertions or removals
	BatchHeaderText    string
	BatchHeaderFields  []*BatchField
	Entries            []*Entry
	BatchControlText   string
	BatchControlFields []*BatchField
	BatchHeaderLine    int
	BatchControlLine   int
}

type BatchField struct {
	PlainText  string
	StyledText string
	*FieldData
	*StyleData
	*SelectionData
	ParentBatch      *Batch
	BatchHeaderLine  int
	BatchControlLine int
}

// NewBatch
func NewBatch(file *File, batchAndEntryRecords []string) *Batch {
	batch := &Batch{
		File: file,
	}

	// Fields are more than just the text field. Fields contains all the contextual
	// data and styling info for a specific Nacha field
	batch.BatchHeaderFields = NewBatchHeaderFields(batchAndEntryRecords[0], batch)

	var entryAndAddendaRecords []string

	for i := 1; i < len(batchAndEntryRecords)-1; i++ {
		record := batchAndEntryRecords[i]
		entryAndAddendaRecords = append(entryAndAddendaRecords, record)

		// Check next record to see if it is an addenda attached to the entry
		nextRecord := batchAndEntryRecords[i+1]
		if nextRecord[0] == '7' {
			entryAndAddendaRecords = append(entryAndAddendaRecords, nextRecord)
			// Fast-forward the counter to the next entry
			i++
		}
		entry := NewEntry(batch, entryAndAddendaRecords)
		batch.Entries = append(batch.Entries, entry)
		entryAndAddendaRecords = []string{} // clear the list of records
	}

	batch.BatchControlFields = NewBatchControlFields(batchAndEntryRecords[len(batchAndEntryRecords)-1], batch)
	return batch
}

func NewBatchHeaderFields(batchHeaderRecord string, parent *Batch) []*BatchField {
	var batchFields []*BatchField

	for _, fc := range BatchHeaderFieldContext {
		batchField := &BatchField{
			ParentBatch: parent,
		}
		batchField.FieldData = NewFieldData(batchField, fc, batchHeaderRecord)
		batchField.StyleData = NewStyleData(fc.Color)
		batchField.SelectionData = new(SelectionData)
		//NewNachifySelection(batchField)
		batchFields = append(batchFields, batchField)
	}
	return batchFields
}

func CreateBatchHeaderLegend() string {
	var legend string
	for _, bh := range BatchHeaderFieldContext {
		legend += " " + ColorText(bh.Label, bh.Color)
	}
	return legend
}

func NewBatchControlFields(batchControlRecord string, parent *Batch) []*BatchField {
	var batchFields []*BatchField

	for _, fc := range BatchControlFieldContext {
		batchField := &BatchField{
			ParentBatch: parent,
		}
		batchField.FieldData = NewFieldData(batchField, fc, batchControlRecord)
		batchField.StyleData = NewStyleData(fc.Color)
		batchField.SelectionData = new(SelectionData)
		//NewNachifySelection(batchField)
		batchFields = append(batchFields, batchField)
	}
	return batchFields
}

func CreateBatchControlLegend() string {
	var legend string
	for _, bc := range BatchControlFieldContext {
		legend += " " + ColorText(bc.Label, bc.Color)
	}
	return legend
}

func (bf *BatchField) RenderStyle() string {
	bf.StyledText = *bf.Value
	if bf.FormatFunc != nil {
		bf.StyledText = bf.FormatFunc(bf.Length, bf.StyledText)
	}
	if bf.SelectionData.SelectionNo != 0 {
		bf.StyledText = fmt.Sprintf(`["%d"]%s[""]`, bf.SelectionData.SelectionNo, bf.StyledText)
	}
	if bf.Effects > 0 {
		effectsPrefix := fmt.Sprintf("[%s:%s:%s%s]", bf.TextColor, bf.BackgroundColor, bf.BoldEffect, bf.UnderlineEffect)
		effectsSuffix := "[-:-:-]"
		bf.StyledText = fmt.Sprintf("%s%s%s", effectsPrefix, bf.StyledText, effectsSuffix)
	}
	return bf.StyledText
}

func (bf *BatchField) RenderPlain() string {
	bf.PlainText = *bf.Value
	if bf.FormatFunc != nil {
		bf.PlainText = bf.FormatFunc(bf.Length, bf.PlainText)
	}
	if bf.SelectionData.SelectionNo != 0 {
		return bf.PlainText
	}
	return ""
}

func (b *Batch) GetBatchFieldByName(name string) (*BatchField, *BatchField) {
	var batchHeaderField *BatchField
	var batchControlField *BatchField

	for _, bhf := range b.BatchHeaderFields {
		if bhf.Name == name {
			batchHeaderField = bhf
			break
		}
		batchHeaderField = nil
	}
	for _, bcf := range b.BatchControlFields {
		if bcf.Name == name {
			batchControlField = bcf
			break
		}
		batchControlField = nil
	}
	return batchHeaderField, batchControlField
}

func (b *Batch) GetEntryByTraceNo(traceNo string) *Entry {
	for _, entry := range b.Entries {
		result := entry.FindEntryFieldByName(TraceNo)
		if result != nil && result.GetTextValue() == traceNo {
			return entry
		}
	}
	return nil
}

func (b *Batch) GetEntryCounts() (totalEntries int, returnEntries int) {
	for _, entry := range b.Entries {
		totalEntries++
		if entry.IsReturn() {
			returnEntries++
		}
	}
	return
}
