package cmd

type TextField interface {
	PlainRender() string
	StyleRender() string
	Exclude() bool
	SetSelectionNo(selectionNo int)
	GetSelectionNo() int
}

type FieldContext struct {
	Name         string
	Label        string
	FlagSet      *bool
	RecordPrefix string
	Position     string
	Color        string
	Definition   string
	Contents     string
	Format       Format
	Tip          []TextField
	Usage        string
}

var FieldIDToContext = map[string]FieldContext{
	"1_RecordTypeCode": {
		Name:         RecordTypeCode,
		Label:        "RecordTypeCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "1",
		Color:        Purple,
		Definition:   "The record type",
		Contents:     NUMERIC,
		Format:       NoPadding,
		Usage:        "Display the field information and definition for RecordTypeCode",
	},
	"1_PriorityCode": {
		Name:         PriorityCode,
		Label:        "PriorityCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "2-3",
		Color:        LtYellow,
		Definition:   "The lower the number, the higher processing priority. Currently, only 01 is used.",
		Contents:     NUMERIC,
		Format:       NoPadding,
		Usage:        "Something about the usage for addenda type",
	},
	"1_ImmediateDestination": {
		Name:         ImmediateDestination,
		Label:        "ImmediateDestination",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "4-13",
		Color:        Cyan,
		Definition: "This field contains the routing number of the ACH operator or receiving point to which the file " +
			"is being transmitted. The 10-character field begins with a blank in the first position, followed by the four-digit" +
			"Federal Reserve Routing Symbol, the four-digit AB institution identifier, and the Check digit (bTTTTAAAAC)\n\n",
		Contents: BTTTTAAAAC,
		Format:   NoPadding,
		Usage:    "Something about the usage for immediate destination type",
	},
	"1_ImmediateOrigin": {
		Name:         ImmediateOrigin,
		Label:        "ImmediateOrigin",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "14-23",
		Color:        Red,
		Contents:     BTTTTAAAAC,
		Definition: "This field contains the routing number of the ACH operator or sending point that is transmitting" +
			"the file. The 10 character field begins with a blank in the first position, followed by the four digit Federal" +
			" Federal Routing symbol, the four digit ABA institution identifier " +
			"and the check digit (bTTTTAAAAC)",
		Usage: "Something about the usage for immediate destination type",
	},
	"1_FileCreationDate": {
		Name:         FileCreationDate,
		Label:        "FileCreationDate",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "24-29",
		Color:        Blue,
		Definition:   "The date on which the file was prepared by the ODFI in the format YYMMDD",
		Contents:     YYMMDD,
		Format:       NoPadding,
		Usage:        "Something about the usage for immediate destination type",
	},
	"1_FileCreationTime": {
		Name:         FileCreationTime,
		Label:        "FileCreationTime",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "30-33",
		Color:        Green,
		Definition:   "The time which the file was prepared by the ODFI in the format HHMM",
		Contents:     HHMM,
		Format:       NoPadding,
		Usage:        "Something about the usage for immediate destination type",
	},
	"1_FileIDModifier": {
		Name:         FileIDModifier,
		Label:        "FileIDModifier",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "34",
		Color:        LtPurple,
		Definition: "Indicates the file's position in relationship to other files that have been sent " +
			"from the ODFI to the ACH operator in a given day. For example, a value of \"A\" would indicate " +
			"this is the first file being sent to the ACH operator on a particular today. Subsequent files, " +
			"if any, would be labeled \"B\", \"C\" etc. The File ID Modifier coupled with the File Creation Date " +
			"and time can be used by the ODFI, along with other information, to trace the file",
		Contents: UPPERALPHAMERIC,
		Format:   NoPadding,
		Usage:    "Something about the usage for addenda type",
	},
	"1_RecordSize": {
		Name:         RecordSize,
		Label:        "RecordSize",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "35-37",
		Color:        SkyBlue,
		Definition:   "Indicates the number of positions in the file record which is always 94 represented at \"094\"",
		Contents:     "094",
		Format:       NoPadding,
		Usage:        "Something about file creation date",
	},
	"1_BlockingFactor": {
		Name:         BlockingFactor,
		Label:        "BlockingFactor",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "38-39",
		Color:        HotPink,
		Definition: "Always set to \"10\", The blocking factor defines the number of records within a block (a block is" +
			"940 characters). For all files moving between a DFI and an ACH operator, the value \"10\" must be used. If" +
			"the number of records within the file is not a multiple of ten, the remainder of the block must be filled with" +
			"9's.",
		Contents: "10",
		Format:   NoPadding,
		Usage:    "Something about file creation date",
	},
	"1_FormatCode": {
		Name:         FormatCode,
		Label:        "FormatCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "40",
		Color:        Orange,
		Definition:   `Always set to "1"`,
		Contents:     "1",
		Format:       NoPadding,
		Usage:        "Something about file creation date",
	},
	"1_ImmediateDestinationName": {
		Name:         ImmediateDestinationName,
		Label:        "ImmediateDestinationName",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "41-63",
		Color:        Yellow,
		Definition: "Name of the financial institution receiving the payment file (Note: does not necessarily indicate final destination) " +
			"\nACH files are first sent to an ACH operator before arriving at the final destination, so the immediate destination is relative to the sender",
		Contents: ALPHAMERIC,
		Format:   RightSpacePadded,
		Usage:    "Something about file creation date",
	},
	"1_ImmediateOriginName": {
		Name:         ImmediateOriginName,
		Label:        "ImmediateOriginName",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "64-86",
		Color:        Purple,
		Definition: "Name of the financial institution sending the payment file. (Note: This does not necessarily mean the originating bank) " +
			"\nACH files are first sent to an ACH operator so the immediate origin is relative to the sender",
		Contents: ALPHAMERIC,
		Format:   RightSpacePadded,
		Usage:    "Something about file creation date",
	},
	"1_ReferenceCode": {
		Name:         ReferenceCode,
		Label:        "ReferenceCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "87-94",
		Color:        LtGreen,
		Definition:   "Optional field which can be used to describe the input file for internal accounting purpose",
		Contents:     ALPHAMERIC,
		Format:       RightSpacePadded, // TODO Double check
		Usage:        "Something about file creation date",
	},
	"5_RecordTypeCode": {
		Name:         RecordTypeCode,
		Label:        "RecordTypeCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "1",
		Color:        Purple,
		Definition: "The record type for a Batch Header. Entries belong to the same batch will share the same" +
			" SEC Code, effective entry date, company ID (sender), and batch descriptor.",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "Display the field information and definition for RecordTypeCode",
	},
	"5_ServiceClassCode": { // TODO: Must match SCC in batch control
		Name:         ServiceClassCode,
		Label:        "ServiceClassCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "2-4",
		Color:        Orange,
		Definition: "Identifies the general classification of dollar entries to be exchanged.\n" +
			"200 - Mixed Debits and Credits\n" +
			"220 - ACH Credits Only\n" +
			"225 - ACH Debits Only\n" +
			"280 - ACH Automated Accounting Advices\n",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "Display the field information and definition for RecordTypeCode",
	},
	"5_CompanyName": { // TODO: Cannot be all spaces or all zeros
		Name:         "COMPANY NAME",
		Label:        "CompanyName",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "5-20",
		Color:        LtGreen,
		Definition:   "GetName of the originator",
		Contents:     ALPHAMERIC,
		Format:       RightSpacePadded,
		Usage:        "Something about the usage for addenda type",
	},
	"5_CompanyDiscretionaryData": {
		Name:         CompanyDiscretionaryData,
		Label:        "CompanyDiscretionaryData",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "21-40",
		Color:        Red,
		Definition:   "Originator/ODFI may include codes of significance only to them to enable specialized handling of all entries within the batch.",
		Contents:     ALPHAMERIC,
		Format:       RightSpacePadded,
		Usage:        "Something about the usage for immediate destination type",
	},
	"5_CompanyID": { // TODO: Cannot be all spaces or all zeros
		Name:         CompanyID,
		Label:        "CompanyID",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "41-50",
		Color:        Blue,
		Definition: "Carries the Originator's unique identification number. ANSI one-digit Identification Code Designators (ICD) " +
			"are used, followed by the nine-digit identification number.",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "Something about the usage for immediate destination type",
	},
	"5_StandardEntryClassCode": {
		Name:         StandardEntryClassCode,
		Label:        "StandardEntryClassCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "51-53",
		Color:        LtYellow,
		Definition: "Contains a three-character code used to identify various types of entries\n" +
			"ACK - ACH Payment Acknowledged\n" +
			"ADV - Automated Accounting Advice\n" +
			"ARC - Accounts Receivable Entry\n" +
			"ATX - Financial EDI Acknowledgment\n" +
			"BOC - Back Office Conversion Entry\n" +
			"CCD - Corporate Credit or Debit Entry\n" +
			"CIE - Customer Initiated Entry\n" +
			"COR - Notification of Change or Refused Notification of Change\n" +
			"CTX - Corporate Trade Exchange\n" +
			"DNE - Death Notification Entry\n" +
			"ENR - Automated Enrollement Entry\n" +
			"IAT - International ACH Transactions\n" +
			"MTE - Machine Transfer Entry\n" +
			"POP - Point of Purpose Entry\n" +
			"POS - Point of Sale Entry\n" +
			"PPD - Prearranged Payment and Deposit Entry\n" +
			"RCK - Re-presented Check Entry\n" +
			"SHR - Shared Network Transaction\n" +
			"TEL - Telephone Initiated Entry\n" +
			"TRC - Check Truncation Entry\n" +
			"TRX - Check Truncation Entries Exchange\n" +
			"WEB - Internet-Initiated/Mobile Entry\n" +
			"XCK - Destroyed Check Entry",
		Contents: UPPERALPHAMERIC,
		Format:   NoPadding,
		Usage:    "Something about the usage for immediate destination type",
	},
	"5_CompanyEntryDescription": { // TODO: Cannot be all spaces or all zeros
		Name:         CompanyEntryDescription,
		Label:        "CompanyEntryDescription",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "54-63",
		Color:        Cyan,
		Definition: "Provides a description of the entry. Certain description are required for certain " +
			"types of entries e.g., PAYROLL, TRANSFER, REVERSAL, RETRY PYMT, RETURN FEE, " +
			"DIRECT PAY",
		Contents: ALPHAMERIC,
		Format:   RightSpacePadded,
		Usage:    "Something about the usage for immediate destination type",
	},
	"5_CompanyDescriptiveDate": {
		Name:         CompanyDescriptiveDate,
		Label:        "CompanyDescriptiveDate",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "64-69",
		Color:        Green,
		Definition: "Except as otherwise noted below, the Originator establishes " +
			"this field as the date it would like to see displayed to the Receiver for " +
			"descriptive purposes. This field is never used to control timing of any computer " +
			"or manual operation. It is solely for descriptive purposes.",
		Contents: ALPHAMERIC,
		Format:   NoPadding,
		Usage:    "Something about the usage for addenda type",
	},
	"5_EffectiveEntryDate": {
		Name:         EffectiveEntryDate,
		Label:        "EffectiveEntryDate",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "70-75",
		Color:        HotPink,
		Definition: "The Banking day specified by the Originator on which it intends a batch of entries to be settled." +
			"\n\nFor credit entries the effective entry date must be either the same banking day as the banking day of processing" +
			" for same day entries, or one or two banking days following the originating ACH operator's processing date for other entries" +
			"\n\nFor debit entries the effective entry date must be either the same banking day as the processing date for same day" +
			" entries or one banking day following the originating ach operator's processing date for other entries.",
		Contents: YYMMDD,
		Format:   NoPadding,
		Usage:    "Something about file creation date",
	},
	"5_SettlementDate": {
		Name:         SettlementDate,
		Label:        "SettlementDate",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "76-78",
		Color:        SkyBlue,
		Definition: "The Settlement Date is inserted in the Company/Batch Header Record by the Receiving ACH Operator. " +
			"It represents the Julian date on which settlement is scheduled to occur for the transactions contained in that batch. " +
			"The Receiving ACH Operator determines the Settlement Date based on the Effective Entry Date and the current ACH processing date",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "Something about file creation date",
	},
	"5_OriginatorStatusCode": {
		Name:         OriginatorStatusCode,
		Label:        "OriginatorStatusCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "1",
		Position:     "79",
		Color:        Yellow,
		Definition: "Refers to the ODFI initiating the entry\n\n" +
			"Code Values:\n" +
			"0 - ADV File prepared by an ACH operator\n" +
			"1 - This code identifies the Originator as a depository institution\n" +
			"2 - This code identifies the Originator as a Federal Government entity or agency",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "Something about file creation date",
	},
	"5_OriginatingDFI": {
		Name:         OriginatingDFI,
		Label:        "OriginatingDFI",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "80-87",
		Color:        LtPurple,
		Definition: "Contains the routing number of the DFI originating the entries within the batch\n\n" +
			"[yellow]RETURNS[white]:\n" +
			"For returns, the ODFI ID is changed to the routing number of the institution initiating" +
			" the return of entry (the RDFI of the original entry)",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "Something about file creation date",
	},
	"5_BatchNo": {
		Name:         BatchNo,
		Label:        "BatchNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "5",
		Position:     "88-94",
		Color:        LtOrange,
		Definition:   "",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "Something about file creation date",
	},
	"6_RecordTypeCode": {
		Name:         RecordTypeCode,
		Label:        "RecordTypeCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "1",
		Color:        Purple,
		Definition:   "A record type code of 6 indicates an entry detail records. Entry details exist with a batch ",
		Contents:     NUMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"6_TransactionCode": {
		Name:         TransactionCode,
		Label:        "TransactionCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "2-3",
		Color:        LtOrange,
		Definition: "Optional field which can be used to describe the input file for internal accounting purpose\n" +
			"Credit - 20, 21, 22, 23, 24, 30, 31, 32, 33, 34\n" +
			"Debit - 25, 26, 27, 28, 35, 36, 37, 38, 39",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "Something about file creation date",
	},
	"6_RdfiRoutingNo": {
		Name:         RdfiRoutingNo,
		Label:        "RdfiRoutingNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "4-11",
		Color:        Cyan,
		Definition: "A nine-digit (including Check digit) identification number used to identify the financial " +
			"institution or bank associated with a customer's bank account.",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"6_CheckDigit": {
		Name:         CheckDigit,
		Label:        "CheckDigit",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "12",
		Color:        Red,
		Definition: "Contains the check digit pertaining to the [::b]routing[::-] number for the DFI at which the account holder" +
			" maintains its account.\n\n" +
			"The Check Digit is computed using Modulus 10 as follows:\n\n" +
			"1. Multiply each digit in the Routing Number by a weighting factor. The weight factors for each digit are:\n\n" +
			"GetPosition 1 2 3 4 5 6 7 8\n" +
			"Weights  3 7 1 3 7 1 3 7\n\n" +
			"2. Add the result of the eight multiplications.\n\n" +
			"3. Subtract the sum from the next highest multiple of 10. The result is the Check Digit\n\n" +
			"Example:\n" +
			"RoutingNo: 0 7  6 4  0 1 2 5\n" +
			"[::u]Mutltiply: 3 7  1 3  7 1 3 7[::-]\n" +
			"      Sum: 0 49 6 12 0 1 6 35 = 109\n\n" +
			"Check Digit = 1 (110 minus 109)",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"6_RDFIAccountNo": {
		Name:         RDFIAccountNo,
		Label:        "RDFIAccountNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "13-29",
		Color:        Blue,
		Definition: "The DFI Account Number is the RDFI's customer's account number. It is usually obtained from" +
			"the on-us field of the MICR line of a check, the account statement, passbook, or other source document provided" +
			"by the RDFI that specifically designates the account number to be used for ACH purposes.\n\n" +
			"If the receivers account number contains more that 17 valid characters, the leftmost 17 characters are inserted into the DFI account" +
			"number field and the remaining characters are truncated. e.g. 012345678901234567 will appear as 0123456780123456." +
			"If it contains fewer than 17 characters, left justify the account number and leave the unused spaces blank. " +
			"Any spaces in the receivers account number should be removed when inserting the account number into the field.",
		Contents: NUMERIC,
		Format:   RightSpacePadded,
		Usage:    "",
	},
	"6_Amount": {
		Name:         Amount,
		Label:        "Amount",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "30-39",
		Color:        Green,
		Definition: "The debit or credit amount in cents to be posted to the Receivers account. The details are indicated by " +
			"the transaction code in the entry detail record.",
		Contents: NUMERIC,
		Format:   LeftZeroPadded,
		Usage:    "",
	},
	"6_IndividualIDNo": {
		Name:         IndividualIDNo,
		Label:        "IndividualIDNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "40-54",
		Color:        LtPurple,
		Definition: "Except as otherwise noted, this field contains the accounting number by which the " +
			"receiver is known to the originator. It is included for further identification and for descriptive purposes." +
			"\n\nThe RDFI should assume no specific format to be present (e.g., presence or absence of dashes) but can assume" +
			" that the field is pre-edited to be suitable for description as is (including blanks and unused positions)\n\n" +
			"IMPORTANT:\n" +
			"For WEB credits, which are sued for P2P entries, the name of the [::bu]consumer[::-] Originator appears in the this field",
		Contents: ALPHAMERIC,
		Format:   RightSpacePadded,
		Usage:    "",
	},
	"6_ReceiverName": {
		Name:         ReceiverName,
		Label:        "ReceiverName",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "",
		Position:     "55-76",
		Color:        Yellow,
		Definition:   "The name of the receiving individual or company.",
		Contents:     ALPHAMERIC,
		Format:       RightSpacePadded,
		Usage:        "",
	},
	"6_DiscretionaryData": {
		Name:         DiscretionaryData,
		Label:        "DiscretionaryData",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "77-78",
		Color:        HotPink,
		Definition: "Option field which lets an ODFI include codes of significance to them, to enable specialized handling of the Entry. " +
			"There is no standardized interpretation for the value of this field. It can be either a single two-character " +
			"code, or two distinct one-character codes, according to the needs of the ODFI and/or Originator involved. This field " +
			"must be intact for any returned entry",
		Usage: "",
	},
	"6_AddendaRecordIndicator": {
		Name:         AddendaRecordIndicator,
		Label:        "AddendaRecordIndicator",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "79",
		Color:        SkyBlue,
		Definition: "Indicates whether an addenda record will follow the entry " +
			"\n0 = none \n1 = One or more (up to 9,999)",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"6_TraceNo": { // TODO: Make sure first 8 digits for traceNo and batch ODFI routing number is the same
		Name:         TraceNo,
		Label:        "TraceNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "6",
		Position:     "80-94",
		Color:        Orange,
		Definition: "Uniquely identifies each Entry Detail Record within a batch in an ACH input File. In association with the Batch Number, " +
			"Transmission (File  Creation) Date, and File ID Modifier, the Trace Number uniquely identifies an Entry " +
			"within a specific File. For Addenda Records, the Trace Number is identical to the Trace Number in the associated Entry Detail Record\n\n" +
			"Positions:\n\n" +
			"01-08 - The first eight positions of the Trace Number in an entry detail record must be the same as the ODFI Routing " +
			"Number in the corresponding Company/Batch Header record\n" +
			"09-15 - Entry Detail Sequence Number - The number assigned in ascending order to each Entry within each batch. Provisions should be made by the ODFI " +
			"to avoid duplication of Trace Numbers if multiple data files are prepared on the same day. Trace Numbers are not required to be contiguous",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"7_RecordTypeCode": {
		Name:         RecordTypeCode,
		Label:        "RecordTypeCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "1",
		Color:        Purple,
		Definition:   "A record type code of 7 indicates an addenda record.",
		Contents:     NUMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"7_AddendaTypeCode": {
		Name:         AddendaTypeCode,
		Label:        "AddendaTypeCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "2-3",
		Color:        Green,
		Definition: "Defines the specific interpretation and format for the addenda information contained " +
			"in the entry",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"7_ReturnReasonCode": {
		Name:         ReturnReasonCode,
		Label:        "ReturnReasonCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "4-6",
		Color:        Red,
		Definition: "3 character code which identifies the reason the entry is being returned\n\n" +
			"R01 - Insufficient Funds\n" +
			"R02 - Account Closed\n" +
			"R03 - Account Not Found\n" +
			"R04 - Invalid Account Number",
		Contents: UPPERALPHAMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"7_DishonoredReturnReasonCode": {
		Name:         DishonoredReturnReasonCode,
		Label:        "DishonoredReturnReasonCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "4-6",
		Color:        Red,
		Definition: "3 character code which identifies the reason the return is being dishonored\n\n" +
			"R61 - Misrouted Return\n" +
			"R62 - Return of Erroneous or Reversing Debit\n" +
			"R67 - Duplicate Return\n" +
			"R68 - Untimely Return\n" +
			"R69 - Field Errors\n" +
			"R68 - Permissible Return Entry Not Accepted/Return Not Requested by ODFI",
		Contents: UPPERALPHAMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"7_ContestedDishonoredReturnReasonCode": {
		Name:         ContestedDishonoredReturnReasonCode,
		Label:        "ContestedDishonoredReturnReasonCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "4-6",
		Color:        Red,
		Definition:   "",
		Contents:     UPPERALPHAMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"7_OriginalEntryTraceNo": {
		Name:         OriginalEntryTraceNo,
		Label:        "OriginalEntryTraceNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "7-21",
		Color:        Orange,
		Definition: "This field contains the Trace Number as originally included on the forward Entry or Prenotification. The RDFI " +
			"must include the Original Entry Trace Number in the Addenda Record of an Entry being returned to an ODFI, in the Addenda Record " +
			"of an NOC, within an Acknowledgement Entry, or with an RDFI request for a copy of an authorization",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"7_ReservedOne": {
		Name:         Reserved,
		Label:        "Reserved",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "22-27",
		Color:        Grey,
		Definition:   "",
		Contents:     ALPHAMERIC,
		Format:       RightSpacePadded,
		Usage:        "",
	},
	"7_OriginalEntryReturnDate": {
		Name:         OriginalEntryReturnDate,
		Label:        "OriginalEntryReturnDate",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "22-27",
		Color:        Grey,
		Definition:   "",
		Contents:     YYMMDD,
		Format:       RightSpacePadded,
		Usage:        "",
	},
	"7_DateOfDeath": {
		Name:         DateOfDeath,
		Label:        "DateOfDeath",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "22-27",
		Color:        Cyan,
		Definition:   "",
		Contents:     YYMMDD,
		Format:       NoPadding,
		Usage:        "",
	},
	"7_OriginalReceivingDFI": {
		Name:         OriginalReceivingDFI,
		Label:        "OriginalReceivingDFI",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "28-35",
		Color:        LtPurple,
		Definition: "Contains the receiving DFI identification as originally included on the forward entry" +
			" or prenotification that the RDFI is returning or correcting. This field must be included in the Addenda " +
			"Record for an Entry being returned fo and ODFI, or within the Addenda Record accompanying a Notification of Change\n\n" +
			"NOTE: This is different from the ORIGINATING DFI Identification found in the batch control record!",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"7_ReservedTwo": {
		Name:         Reserved,
		Label:        "Reserved",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "36-38",
		Color:        Grey,
		Definition:   "",
		Contents:     ALPHAMERIC,
		Format:       RightSpacePadded,
		Usage:        "",
	},
	"7_OriginalSettlementDate": {
		Name:         OriginalSettlementDate,
		Label:        "OriginalSettlementDate",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "36-38",
		Color:        Yellow,
		Definition:   "",
		Contents:     NUMERIC,
		Format:       RightSpacePadded,
		Usage:        "",
	},
	"7_AddendaInfoOne": {
		Name:         AddendaInfo,
		Label:        "AddendaInfo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "36-79",
		Color:        Blue,
		Definition:   "",
		Contents:     ALPHAMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"7_ReturnTraceNo": {
		Name:         ReturnTraceNo,
		Label:        "ReturnTraceNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "39-53",
		Color:        Blue,
		Definition:   "",
		Contents:     ALPHAMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"7_ReturnSettlementDate": {
		Name:         ReturnSettlementDate,
		Label:        "ReturnSettlementDate",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "54-56",
		Color:        Green,
		Definition: "The Settlement Date of the return being dishnored. " +
			"It represents the Julian date on which settlement was scheduled to occur for the return",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "Something about file creation date",
	},
	"7_ReturnReasonCodeDishonored": {
		Name:         ReturnReasonCode,
		Label:        "ReturnReasonCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "57-58",
		Color:        Red,
		Definition: "3 character code which identifies the reason the entry is being returned\n\n" +
			"R01 - Insufficient Funds\n" +
			"R02 - Account Closed\n" +
			"R03 - Account Not Found\n" +
			"R04 - Invalid Account Number",
		Contents: UPPERALPHAMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"7_DishonoredReturnTraceNo": {
		Name:         DishonoredReturnTraceNo,
		Label:        "DishonoredReturnTraceNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "39-53",
		Color:        Blue,
		Definition:   "",
		Contents:     ALPHAMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"7_DishonoredReturnSettlementDate": {
		Name:         DishonoredReturnSettlementDate,
		Label:        "DishonoredReturnSettlementDate",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "54-56",
		Color:        Green,
		Definition: "The Settlement Date of the return being dishnored. " +
			"It represents the Julian date on which settlement was scheduled to occur for the return",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "Something about file creation date",
	},
	"7_DishonoredReturnReasonCodeDishonored": {
		Name:         DishonoredReturnReasonCode,
		Label:        "DishonoredReturnReasonCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "57-58",
		Color:        Red,
		Definition:   "",
		Contents:     UPPERALPHAMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"7_AddendaInfoDishonored": {
		Name:         AddendaInfo,
		Label:        "AddendaInfo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "59-79",
		Color:        Blue,
		Definition:   "",
		Contents:     ALPHAMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"7_ReservedThree": {
		Name:         Reserved,
		Label:        "Reserved",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "79",
		Color:        Grey,
		Definition:   "",
		Contents:     ALPHAMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"7_TraceNo": {
		Name:         TraceNo,
		Label:        "TraceNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "7",
		Position:     "80-94",
		Color:        Yellow,
		Definition: "Uniquely identifies each Entry Detail Record within a " +
			"batch in an ACH input File. In association with the Batch Number, Transmission " +
			"(File  Creation) Date, and File ID Modifier, the Trace Number uniquely identifies an Entry " +
			"within a specific File. For Addenda Records, the Trace Number is identical to the Trace " +
			"Number in the associated Entry Detail Record",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"8_RecordTypeCode": {
		Name:         RecordTypeCode,
		Label:        "RecordTypeCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "1",
		Color:        Purple,
		Definition:   "",
		Contents:     NUMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"8_ServiceClassCode": { //TODO Match SeviceClassCode in the Batch Header
		Name:         ServiceClassCode,
		Label:        "ServiceClassCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "2-4",
		Color:        Yellow,
		Definition: "Identifies the general classification of dollar entries to be exchanged." +
			"Code Values:" +
			"200 - Mixed Debits and Credits" +
			"220 - ACH Credits Only" +
			"225 - ACH Debits Only" +
			"280 - ACH Automated Accounting Advices",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"8_EntryAddendaCount": {
		Name:         EntryAddendaCount,
		Label:        "EntryAddendaCount",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "5-10",
		Color:        Green,
		Definition:   "The total number of Entry Detail Records and Addenda Records processed within the batch",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "",
	},
	"8_EntryHash": {
		Name:         EntryHash,
		Label:        "EntryHash",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "11-20",
		Color:        Blue,
		Definition: "The sum of all the Receiving DFI ID fields contained within the Entry Detail Records" +
			"in a batch. The receiving DFI ID field contains the 8-digit routing number of the RDFI. The hash" +
			"is the sum of the 8-digit routing numbers",
		Contents: NUMERIC,
		Format:   LeftZeroPadded,
		Usage:    "",
	},
	"8_TotalDebitAmount": {
		Name:         TotalDebitAmount,
		Label:        "TotalDebitAmount",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "21-32",
		Color:        Red,
		Definition:   "Sum of all debits in the batches corresponding entry detail records",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "",
	},
	"8_TotalCreditAmount": {
		Name:         TotalCreditAmount,
		Label:        "TotalCreditAmount",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "33-44",
		Color:        Orange,
		Definition:   "Sum of all credits in the batches corresponding entry detail records",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "",
	},
	"8_CompanyID": {
		Name:         CompanyID,
		Label:        "CompanyID",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "45-54",
		Color:        Cyan,
		Definition:   "A 10-digit unique identifier used by the ODFI to identify the individual or company originating the ACH transaction.",
		Contents:     NUMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"8_MessageAuthenticationCode": {
		Name:         MessageAuthenticationCode,
		Label:        "MessageAuthenticationCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "55-73",
		Color:        LtPurple,
		Definition:   "",
		Contents:     ALPHAMERIC,
		Format:       RightSpacePadded,
		Usage:        "",
	},
	"8_Reserved": {
		Name:         Reserved,
		Label:        "Reserved",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "74-79",
		Color:        Grey,
		Definition:   "",
		Contents:     ALPHAMERIC,
		Format:       RightSpacePadded,
		Usage:        "",
	},
	"8_OriginatingDFI": {
		Name:         OriginatingDFI,
		Label:        "OriginatingDFI",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "80-87",
		Color:        LtYellow,
		Definition: "The routing number of the DFI originating the entries within the batch.\n\n" +
			"NOTE: This is different from the Original Receiving DFI found in an Addenda record!",
		Contents: NUMERIC,
		Format:   NoPadding,
		Usage:    "",
	},
	"8_BatchNo": {
		Name:         BatchNo,
		Label:        "BatchNo",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "8",
		Position:     "88-94",
		Color:        LtGreen,
		Definition:   "",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "",
	},
	"9_RecordTypeCode": {
		Name:         RecordTypeCode,
		Label:        "RecordTypeCode",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "9",
		Position:     "1",
		Color:        Purple,
		Definition:   "A Record Type Code of 9 indicates a File Control Record. There is only 1 File Control per ach file",
		Contents:     NUMERIC,
		Format:       NoPadding,
		Usage:        "",
	},
	"9_BatchCount": {
		Name:         BatchCount,
		Label:        "BatchCount",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "9",
		Position:     "2-7",
		Color:        LtGreen,
		Definition:   "The total number of batches in the file",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "",
	},
	"9_BlockCount": {
		Name:         BlockCount,
		Label:        "BlockCount",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "9",
		Position:     "8-13",
		Color:        Orange,
		Definition:   "The total number of blocks in the file",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "",
	},
	"9_EntryAddendaCount": {
		Name:         EntryAddendaCount,
		Label:        "EntryAddendaCount",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "9",
		Position:     "14-21",
		Color:        Red,
		Definition:   "The total number of Entry Detail Records and Addenda Records within the file",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "",
	},
	"9_EntryHash": {
		Name:         EntryHash,
		Label:        "EntryHash",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "9",
		Position:     "22-31",
		Color:        Blue,
		Definition: "The sum of the entry hash fields contained within the company batch records of the file." +
			"If the sum exceeds 10 characters the field must be populated with the rightmost 10 characters." +
			"For example, if the sum of the RDFI ID fields within a batch is 123456789012, the Entry hash would" +
			"be populated with 3456789012",
		Contents: NUMERIC,
		Format:   LeftZeroPadded,
		Usage:    "",
	},
	"9_TotalFileDebitAmount": {
		Name:         TotalFileDebitAmount,
		Label:        "TotalFileDebitAmount",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "9",
		Position:     "32-43",
		Color:        Green,
		Definition:   "Sum of all debits in the corresponding batches",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "",
	},
	"9_TotalFileCreditAmount": {
		Name:         TotalFileCreditAmount,
		Label:        "TotalFileCreditAmount",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "9",
		Position:     "44-55",
		Color:        Cyan,
		Definition:   "Sum of all credits in the corresponding batches",
		Contents:     NUMERIC,
		Format:       LeftZeroPadded,
		Usage:        "",
	},
	"9_Reserved": {
		Name:         Reserved,
		Label:        "Reserved",
		FlagSet:      NewBoolPointer(),
		RecordPrefix: "9",
		Position:     "56-94",
		Color:        Grey,
		Definition:   "Reserved field that is typically left blank of unused",
		Contents:     ALPHAMERIC,
		Format:       RightSpacePadded,
		Usage:        "",
	},
}

func NewBoolPointer() *bool {
	x := false
	return &x
}
