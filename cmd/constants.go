package cmd

const (
	// FIELD CONTENT TYPES

	ALPHAMERIC      string = "Alphameric"
	NUMERIC         string = "Numeric"
	BTTTTAAAAC      string = "bTTTTAAAAC"
	YYMMDD          string = "YYMMDD"
	HHMM            string = "HHMM"
	UPPERALPHAMERIC        = "Uppercase Alphameric"

	// FILE HEADER FIELDS

	RecordTypeCode           = "RECORD TYPE CODE"
	PriorityCode             = "PRIORITY CODE"
	ImmediateDestination     = "IMMEDIATE DESTINATION"
	ImmediateOrigin          = "IMMEDIATE ORIGIN"
	FileCreationDate         = "FILE CREATION DATE"
	FileCreationTime         = "FILE CREATION TIME"
	FileIDModifier           = "FILE ID MODIFIER"
	RecordSize               = "RECORD SIZE"
	BlockingFactor           = "BLOCKING FACTOR"
	FormatCode               = "FORMAT CODE"
	ImmediateDestinationName = "IMMEDIATE DESTINATION NAME"
	ImmediateOriginName      = "IMMEDIATE ORIGIN NAME"
	ReferenceCode            = "REFERENCE CODE"

	// BATCH HEADER FIELDS

	ServiceClassCode         = "SERVICE CLASS CODE"
	CompanyName              = "COMPANY NAME"
	CompanyDiscretionaryData = "COMPANY DISCRETIONARY DATA"
	CompanyID                = "COMPANY ID"
	StandardEntryClassCode   = "STANDARD ENTRY CLASS CODE"
	CompanyEntryDescription  = "COMPANY ENTRY DESCRIPTION"
	CompanyDescriptiveDate   = "COMPANY DESCRIPTIVE DATE"
	EffectiveEntryDate       = "EFFECTIVE ENTRY DATE"
	SettlementDate           = "SETTLEMENT DATE"
	OriginatorStatusCode     = "ORIGINATOR STATUS CODE"
	OriginatingDFI           = "ORIGINATING DFI"
	BatchNo                  = "BATCH NUMBER"

	// ENTRY FIELDS

	TransactionCode        = "TRANSACTION CODE"
	RdfiRoutingNo          = "RDFI ROUTING NUMBER"
	CheckDigit             = "CHECK DIGIT"
	RDFIAccountNo          = "RDFI ACCOUNT NUMBER"
	Amount                 = "AMOUNT"
	IndividualIDNo         = "INDIVIDUAL ID NUMBER"
	ReceiverName           = "RECEIVER NAME"
	DiscretionaryData      = "DISCRETIONARY DATA"
	AddendaRecordIndicator = "ADDENDA RECORD INDICATOR"
	TraceNo                = "TRACE NUMBER"

	// ADDENDA FIELDS

	AddendaTypeCode                     = "ADDENDA TYPE CODE"
	DishonoredReturnReasonCode          = "DISHONORED RETURN REASON CODE"
	ContestedDishonoredReturnReasonCode = "CONTESTED DISHONORED RETURN REASON CODE"
	OriginalEntryTraceNo                = "ORIGINAL ENTRY TRACE NUMBER"
	OriginalEntryReturnDate             = "ORIGINAL ENTRY RETURN DATE"
	OriginalSettlementDate              = "ORIGINAL SETTLEMENT DATE"
	DateOfDeath                         = "DATE OF DEATH"
	OriginalReceivingDFI                = "ORIGINAL RECEIVING DFI"
	AddendaInfo                         = "ADDENDA INFO"
	ReturnSettlementDate                = "RETURN SETTLEMENT DATE"
	ReturnReasonCode                    = "RETURN REASON CODE"
	ReturnTraceNo                       = "RETURN TRACE NUMBER"
	DishonoredReturnSettlementDate      = "DISHONORED RETURN SETTLEMENT DATE"
	DishonoredReturnTraceNo             = "DISHONORED RETURN TRACE NUMBER"

	// BATCH CONTROL FIELDS

	EntryAddendaCount         = "ENTRY ADDENDA COUNT"
	EntryHash                 = "ENTRY HASH"
	TotalDebitAmount          = "TOTAL DEBIT AMOUNT"
	TotalCreditAmount         = "TOTAL CREDIT AMOUNT"
	MessageAuthenticationCode = "MESSAGE AUTHENTICATION CODE"
	Reserved                  = "RESERVED"

	// FILE CONTROL FIELDS

	BatchCount            = "BATCH COUNT"
	BlockCount            = "BLOCK COUNT"
	TotalFileDebitAmount  = "TOTAL FILE DEBIT AMOUNT"
	TotalFileCreditAmount = "TOTAL FILE CREDIT AMOUNT"
)

type Format string

const (
	RightSpacePadded Format = "RightSpacePadded"
	LeftZeroPadded   Format = "LeftZeroPadded"
	NoPadding        Format = ""
)

type DCSign string

const (
	DEBIT  DCSign = "Debit"
	CREDIT DCSign = "Credit"
)
