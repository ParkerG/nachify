package cmd

import (
	"bufio"
	"fmt"
	"strings"
)

const (
	TransactionCodeIndex = iota + 1
	RDFIRoutingNoIndex
	_
	RDFIAccountNoIndex
	AmountIndex
	IndividualIDNoIndex
	ReceiverNameIndex
	_
	AddendaIndicatorIndex
	EntryTraceNoIndex
)

var EntryFieldContext = []FieldContext{
	FieldIDToContext["6_RecordTypeCode"],
	FieldIDToContext["6_TransactionCode"],
	FieldIDToContext["6_RdfiRoutingNo"],
	FieldIDToContext["6_CheckDigit"],
	FieldIDToContext["6_RDFIAccountNo"],
	FieldIDToContext["6_Amount"],
	FieldIDToContext["6_IndividualIDNo"],
	FieldIDToContext["6_ReceiverName"],
	FieldIDToContext["6_DiscretionaryData"],
	FieldIDToContext["6_AddendaRecordIndicator"],
	FieldIDToContext["6_TraceNo"],
}

type Entry struct {
	Batch         *Batch
	BatchPosition int // Needed to perform new entry insertions
	EntryText     string
	EntryFields   []*EntryField
	Addenda99     *Addenda99
	LineNumber    int
}

type EntryField struct {
	ParentEntry *Entry
	PlainText   string
	StyledText  string
	*FieldData
	*StyleData
	*SelectionData
	BatchHeaderLine  int
	BatchControlLine int
}

func NewEntry(batch *Batch, entryRecord []string) *Entry {
	entry := &Entry{
		Batch:     batch,
		EntryText: entryRecord[0],
	}

	entry.EntryFields = NewEntryFields(entryRecord[0], entry)
	if len(entryRecord) > 1 {
		switch entryRecord[1][1:3] {
		case "99":
			entry.Addenda99 = NewAddenda99(entry, entryRecord[1])
		}
	}
	return entry
}

func CreateEntryLegend() string {
	var legend string
	for _, ef := range EntryFieldContext {
		legend += " " + ColorText(ef.Label, ef.Color)
	}
	return legend
}

func (e *Entry) FindEntryFieldByName(name string) *EntryField {
	for _, ef := range e.EntryFields {
		if ef.Name == name {
			return ef
		}
	}
	return nil
}

func (e *Entry) EntryFieldValueByIndex(index int) string {
	return e.EntryFields[index].GetTextValue()
}

func (e *Entry) DCSign() DCSign {
	ef := e.FindEntryFieldByName(TransactionCode)

	switch *ef.Value {
	case "20", "21", "22", "23", "24", "30", "31", "32", "33", "34":
		return CREDIT
	case "25", "26", "27", "28", "35", "36", "37", "38", "39":
		return DEBIT
	}
	return DEBIT
}

func GetDCSignByCode(code string) DCSign {
	switch code {
	case "20", "21", "22", "23", "24", "30", "31", "32", "33", "34":
		return CREDIT
	case "25", "26", "27", "28", "35", "36", "37", "38", "39":
		return DEBIT
	}
	return DEBIT
}

func (e *Entry) ReassignDCLinks(code string) {
	oldSign := e.DCSign()
	newSign := GetDCSignByCode(code)

	if oldSign == newSign {
		return
	}

	entryAmountField := e.FindEntryFieldByName(Amount)

	batch := e.Batch
	_, batchCreditField := batch.GetBatchFieldByName(TotalCreditAmount)
	_, batchDebitField := batch.GetBatchFieldByName(TotalDebitAmount)

	if oldSign == CREDIT {
		UnlinkAggregates(batchCreditField, entryAmountField)
		LinkAggregates(batchDebitField, entryAmountField)
	} else if oldSign == DEBIT {
		UnlinkAggregates(batchDebitField, entryAmountField)
		LinkAggregates(batchCreditField, entryAmountField)
	}
}

func (e *Entry) IsReturn() bool {
	return e.Addenda99 != nil
}

func NewEntryFields(entryRecord string, parent *Entry) []*EntryField {
	var entryFields []*EntryField
	for _, ec := range EntryFieldContext {
		entryField := &EntryField{
			ParentEntry: parent,
		}
		entryField.FieldData = NewFieldData(entryField, ec, entryRecord)
		entryField.StyleData = NewStyleData(ec.Color)
		entryField.SelectionData = new(SelectionData)
		//NewNachifySelection(entryField)
		entryFields = append(entryFields, entryField)
	}
	return entryFields
}

func (ef *EntryField) RenderStyle() string {
	ef.StyledText = *ef.Value
	if ef.FormatFunc != nil {
		ef.StyledText = ef.FormatFunc(ef.Length, ef.StyledText)
	}
	if ef.SelectionData.SelectionNo != 0 {
		ef.StyledText = fmt.Sprintf(`["%d"]%s[""]`, ef.SelectionData.SelectionNo, ef.StyledText)
	}
	if ef.Effects > 0 {
		effectsPrefix := fmt.Sprintf("[%s:%s:%s%s]", ef.TextColor, ef.BackgroundColor, ef.BoldEffect, ef.UnderlineEffect)
		effectsSuffix := "[-:-:-]"
		ef.StyledText = fmt.Sprintf("%s%s%s", effectsPrefix, ef.StyledText, effectsSuffix)
	}
	return ef.StyledText
}

func (ef *EntryField) RenderPlain() string {
	ef.PlainText = *ef.Value
	if ef.FormatFunc != nil {
		ef.PlainText = ef.FormatFunc(ef.Length, ef.PlainText)
	}
	if ef.SelectionData.SelectionNo != 0 {
		return ef.PlainText
	}
	return ""
}

func (ef *EntryField) RenderPlainWithSelection() string {
	ef.PlainText = *ef.Value
	if ef.FormatFunc != nil {
		ef.PlainText = ef.FormatFunc(ef.Length, ef.PlainText)
	}
	if ef.SelectionData.SelectionNo != 0 {
		ef.PlainText = fmt.Sprintf(`["%d"]%s[""]`, ef.SelectionData.SelectionNo, ef.PlainText)
	}
	return ef.PlainText
}

func (ef *EntryField) NewFileWithSingleEntry() *File {
	entryLineNo := ef.LineNumber

	batch := ef.ParentEntry.Batch
	file := batch.File
	batchHeaderLinNo := batch.BatchHeaderLine
	batchControlLinNo := batch.BatchControlLine
	fileHeaderLineNo := file.FileHeaderLine
	fileControlLineNo := file.FileControlLine

	var newLines []string

	newLines = append(newLines, controller.GetStringRecordByLine(fileHeaderLineNo))
	newLines = append(newLines, controller.GetStringRecordByLine(batchHeaderLinNo))
	newLines = append(newLines, controller.GetStringRecordByLine(entryLineNo))
	newLines = append(newLines, controller.GetStringRecordByLine(batchControlLinNo))
	newLines = append(newLines, controller.GetStringRecordByLine(fileControlLineNo))
	document := strings.Join(newLines, "\n")
	reader := strings.NewReader(document)
	s := bufio.NewScanner(reader)
	newFile := NewFile(s)

	newEntry := newFile.Batches[0].Entries[0].EntryFields[0]
	AddAddenda99(newEntry)
	return newFile
}

func (ef *EntryField) InsertNewEntry() *File {
	entryLineNo := ef.LineNumber

	batch := ef.ParentEntry.Batch
	file := batch.File
	batchHeaderLinNo := batch.BatchHeaderLine
	batchControlLinNo := batch.BatchControlLine
	fileHeaderLineNo := file.FileHeaderLine
	fileControlLineNo := file.FileControlLine

	var newLines []string

	newLines = append(newLines, controller.GetStringRecordByLine(fileHeaderLineNo))
	newLines = append(newLines, controller.GetStringRecordByLine(batchHeaderLinNo))
	newLines = append(newLines, controller.GetStringRecordByLine(entryLineNo))
	newLines = append(newLines, controller.GetStringRecordByLine(entryLineNo))
	newLines = append(newLines, controller.GetStringRecordByLine(batchControlLinNo))
	newLines = append(newLines, controller.GetStringRecordByLine(fileControlLineNo))
	document := strings.Join(newLines, "\n")
	reader := strings.NewReader(document)
	s := bufio.NewScanner(reader)
	newFile := NewFile(s)

	return newFile
}
